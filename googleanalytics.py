from apiclient.discovery import build
from oauth2client.service_account import ServiceAccountCredentials
import pandas as pd

def ga_response_dataframe(response):
    row_list = []
    # Get each collected report
    for report in response.get('reports', []):
        # Set column headers
        column_header = report.get('columnHeader', {})
        dimension_headers = column_header.get('dimensions', [])
        metric_headers = column_header.get('metricHeader', {}).get('metricHeaderEntries', [])
    
        # Get each row in the report
        for row in report.get('data', {}).get('rows', []):
            # create dict for each row
            row_dict = {}
            dimensions = row.get('dimensions', [])
            date_range_values = row.get('metrics', [])

            # Fill dict with dimension header (key) and dimension value (value)
            for header, dimension in zip(dimension_headers, dimensions):
                row_dict[header] = dimension

            # Fill dict with metric header (key) and metric value (value)
            for i, values in enumerate(date_range_values):
                for metric, value in zip(metric_headers, values.get('values')):
                # Set int as int, float a float
                    if ',' in value or '.' in value:
                        row_dict[metric.get('name')] = float(value)
                    else:
                        row_dict[metric.get('name')] = int(value)

            row_list.append(row_dict)
    return pd.DataFrame(row_list)


def googleanalytics():

    SCOPES = ['https://www.googleapis.com/auth/analytics.readonly']
    KEY_FILE_LOCATION = 'static/get data-954758f90b2a.json'
    VIEW_ID = '233430041'

    credentials = ServiceAccountCredentials.from_json_keyfile_name(KEY_FILE_LOCATION, SCOPES)

    # Build the service object.
    analytics = build('analyticsreporting', 'v4', credentials=credentials)
    # print(analytics.reports().batchGet())

    ###  Ville des utilisateurs, dérivée de leurs adresses IP ou ID géographiques.
    ResponseCity = analytics.reports().batchGet(body={
        'reportRequests': [{
            'viewId': VIEW_ID,
            'dateRanges': [{'startDate': '2020-11-21', 'endDate': 'today'}],
            'metrics': [
                {"expression": "ga:pageviews"},
                {"expression": "ga:avgSessionDuration"},
                {"expression": "ga:users"}
                # {"expression": "ga:avgPageLoadTime"}
            ], "dimensions": [
                {"name": "ga:city"}
            ]
        }]}).execute()

    #### Un booléen, nouveau visiteur ou visiteur de retour, indiquant si les utilisateurs sont nouveaux ou reviennent.
    ResponseTypeUser = analytics.reports().batchGet(body={
        'reportRequests': [{
            'viewId': VIEW_ID,
            'dateRanges': [{'startDate': '2020-11-21', 'endDate': 'today'}],
            'metrics': [
                {"expression": "ga:pageviews"},
                {"expression": "ga:avgSessionDuration"},
                {"expression": "ga:users"}
                # {"expression": "ga:avgPageLoadTime"}
            ], "dimensions": [
                {"name": "ga:userType"}
            ]
        }]}).execute()
    # print(response_city)

    ### The date of the session formatted as YYYYMMDD.
    ResponseDate = analytics.reports().batchGet(body={
        'reportRequests': [{
            'viewId': VIEW_ID,
            'dateRanges': [{'startDate': '2020-11-21', 'endDate': 'today'}],
            'metrics': [
                {"expression": "ga:pageviews"},
                {"expression": "ga:avgSessionDuration"},
                {"expression": "ga:users"}
                # {"expression": "ga:avgPageLoadTime"}
            ], "dimensions": [
                {"name": "ga:date"}
            ]
        }]}).execute()

    ###  Data géographique

    ResponseGeo = analytics.reports().batchGet(body={
        'reportRequests': [{
            'viewId': VIEW_ID,
            'dateRanges': [{'startDate': '2020-11-18', 'endDate': 'today'}],
            'metrics': [
                {"expression": "ga:sessions"},
            ], "dimensions": [
                {"name": "ga:longitude"},
                {"name": "ga:latitude"},
                {"name": "ga:city"}
            ], "samplingLevel": "LARGE",
            "pageSize": 10000
        }]}).execute()

    
    

    # # %matplotlib inline
    # import matplotlib.pyplot as plt
    # plt.style.use('ggplot')

    dfCity = ga_response_dataframe(ResponseCity)
    dfTypeUser = ga_response_dataframe(ResponseTypeUser)
    dfDate = ga_response_dataframe(ResponseDate)
    dfGeo = ga_response_dataframe(ResponseGeo)
    dfGeo['ga:latitude'] = pd.to_numeric(dfGeo['ga:latitude'])
    dfGeo['ga:longitude'] = pd.to_numeric(dfGeo['ga:longitude'])

    return dfCity, dfTypeUser, dfDate, dfGeo
