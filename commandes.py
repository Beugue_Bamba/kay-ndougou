# coding=utf-8

from sqlalchemy import Column, String, Integer, Numeric, Date, ForeignKey
from sqlalchemy.orm import relationship

from basepoissons import Base

class Commandes(Base):
    __tablename__ = 'commandes'

    id = Column(Integer, primary_key=True)
    
    prenom_client = Column(String)
    nom_client = Column(String)
    num_client = Column(String)
    date_commande = Column(Date)
    total = Column(Numeric)
    frais_livraison = Column(String)
    adresslivraison = Column(String)
    date_a_livrer = Column(String)
    appeller = Column(String)
    livrer = Column(String)
    provenance = Column(String)   # en_ligne ou enregistrer
    valider = Column(String) # oui ou non  # comptable valide par jour les commandes enregistrées par le caissier
    depense = Column(Numeric)
    benefice = Column(Numeric)
    valunique = Column(String)
    user_id = Column(Integer, ForeignKey('users.id'))
    user = relationship("User", backref="modeles")
    boutique_id = Column(Integer, ForeignKey('boutique.id'))
    boutique = relationship("Boutiques", backref="modeles")

    
    
    
    


    def __init__(self, prenom_client, nom_client, num_client, date_commande,total, frais_livraison,adresslivraison,date_a_livrer,appeller, livrer,provenance,valider,depense, benefice, valunique, user, boutique):
        self.prenom_client = prenom_client
        self.nom_client = nom_client
        self.num_client = num_client
        self.date_commande = date_commande
        self.total = total
        self.frais_livraison = frais_livraison
        self.adresslivraison = adresslivraison
        self.date_a_livrer = date_a_livrer
        self.appeller = appeller
        self.livrer = livrer
        self.provenance = provenance
        self.valider = valider
        self.valunique = valunique
        self.user = user
        self.depense = depense
        self.benefice = benefice
        self.boutique = boutique
