# coding=utf-8

from sqlalchemy import Column, String, Integer, Numeric, Date, ForeignKey
from sqlalchemy.orm import relationship

from basepoissons import Base

class Boutiques(Base):
    __tablename__ = 'boutique'

    id = Column(Integer, primary_key=True)
    nomboutique = Column(String)
    adressboutique = Column(String)
    responsableboutique = Column(String)
    telephoneresponsableboutique = Column(String)
    mailresponsableboutique = Column(String)
    adressresponsableboutique = Column(String)
    

    


    def __init__(self, nomboutique, adressboutique, responsableboutique, telephoneresponsableboutique, mailresponsableboutique, adressresponsableboutique):
        self.nomboutique = nomboutique
        self.adressboutique = adressboutique
        self.responsableboutique = responsableboutique
        self.telephoneresponsableboutique = telephoneresponsableboutique
        self.mailresponsableboutique = mailresponsableboutique
        self.adressresponsableboutique = adressresponsableboutique
