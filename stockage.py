# coding=utf-8

from sqlalchemy import Column, String, Integer, Numeric, Date, ForeignKey
from sqlalchemy.orm import relationship

from basepoissons import Base

class Stockages(Base):
    __tablename__ = 'stockages'

    id = Column(Integer, primary_key=True)
    boutique = Column(String)
    quantitestocker = Column(Numeric)
    categorieprod = Column(String)
    nomprod = Column(String)
    puachat = Column(Numeric)
    puvente = Column(Numeric)
    benefice = Column(Numeric)
    commande = Column(Integer)
    user = Column(String)
    types = Column(String) #achat, vente
    statut = Column(String) # enattente, encours, finish, enattenteold, encoursold
    Date = Column(Date)
    

    


    def __init__(self, boutique, quantitestocker, categorieprod, nomprod, puachat, puvente, benefice,commande, user, types,statut, Date):
        self.boutique = boutique
        self.quantitestocker = quantitestocker
        self.categorieprod = categorieprod
        self.nomprod = nomprod
        self.puachat = puachat
        self.puvente = puvente
        self.benefice = benefice
        self.commande = commande
        self.user = user
        self.types = types
        self.statut = statut
        self.Date = Date
        
