# coding=utf-8

from sqlalchemy import Column, String, Integer, Numeric, Date, ForeignKey

from basepoissons import Base


class Djeun(Base):
    __tablename__ = 'djieun'

    id = Column(Integer, primary_key=True)
    nom = Column(String)
    photo = Column(String)
    prix = Column(Numeric)
    old_prix = Column(String)
    type_commande = Column(String)  #viandes, poissons, poulets, dindes, pigeons, crevettes
    description = Column(String)
    valeur = Column(String)


    def __init__(self, nom, photo, prix, old_prix, type_commande, description, valeur):
        self.photo = photo
        self.nom = nom
        self.prix = prix
        self.old_prix = old_prix
        self.type_commande = type_commande
        self.description = description
        self.valeur = valeur
        
