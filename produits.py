# coding=utf-8

from sqlalchemy import Column, String, Integer, Numeric, Date, ForeignKey
from sqlalchemy.orm import relationship

from basepoissons import Base

class Produits(Base):
    __tablename__ = 'produits'

    id = Column(Integer, primary_key=True)
    nom = Column(String)
    photo = Column(String)
    prix_unitaire = Column(Numeric)
    quantite = Column(Numeric)
    prix_total = Column(Numeric)
    fraislivraison = Column(String)
    tot_prix_pro = Column(Numeric)
    commandes_id = Column(Integer, ForeignKey('commandes.id'))
    commandes = relationship("Commandes", backref="modeles")
    categories = Column(String)

    


    def __init__(self, nom, photo, prix_unitaire, quantite, prix_total, fraislivraison,tot_prix_pro, commandes, categories):
        self.nom = nom
        self.photo = photo
        self.prix_unitaire = prix_unitaire
        self.quantite = quantite
        self.prix_total = prix_total
        self.fraislivraison = fraislivraison
        self.tot_prix_pro = tot_prix_pro
        self.commandes = commandes
        self.categories = categories
