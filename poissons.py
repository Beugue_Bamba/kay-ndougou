# import flask_login
from flask import Flask, render_template, redirect, url_for, request, flash, session, g,jsonify
import psycopg2
import datetime
import re  # verification des mails, noms, etc
from werkzeug.security import generate_password_hash
from werkzeug.security import check_password_hash, check_password_hash
import datetime
from PIL import Image
# from resizeimage import resizeimage

import os
from werkzeug.utils import secure_filename
import pandas as pd
import numpy as np
from flask_paginate import Pagination, get_page_args
from flask_sqlalchemy import SQLAlchemy

from flask_cors import CORS
from sqlalchemy import or_
# from flask_caching import Cache




# 1 - imports tables of data base
from datetime import date

from basepoissons import Sessionalchemy, engine, Base
from produits import Commande
from users import User
from djeun import Djeun

# 2 - generer le shema la base de données
Base.metadata.create_all(engine)
######################################FLASK MIGRATE###################################
from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand
#######################################################################################



###################### CREATION DE MOT DE PASS ALEATOIRE ##################
import string
from random import sample

app = Flask(__name__)
CORS(app)

# define the cache config keys, remember that it can be done in a settings file
app.config['CACHE_TYPE'] = 'simple'
# register the cache instance and binds it on to your app 
# app.cache = Cache(app) 

HOSTNAME = '127.0.0.1'
PORT = '5432'
DATABASE = 'dbpoissons'
USERNAME = 'postgres'
PASSWORD = 'Kh@dim1991'


app.config["IMAGE_UPLOADS"] ="/home/mamadou/Bureau/PROJETS/Poissons/static/images"
app.config["ALLOWED_IMAGE_EXTENSIONS"] = ["JPEG", "JPG", "PNG", "GIF", "tiff", "HEIF", "HEIC"]
ALLOWED_EXTENSIONS = set([ 'png', 'jpg', 'jpeg', 'gif', 'tiff', 'heif', 'heic'])

app.config["MAX_IMAGE_FILESIZE"] = 0.5 * 4050 * 4050
app.config['SQLALCHEMY_DATABASE_URI']="postgres+psycopg2://{}:{}@{}:{}/{}".format (USERNAME,PASSWORD,HOSTNAME,PORT,DATABASE)
Sessionalchemy = Sessionalchemy()

db = SQLAlchemy(app)

migrate = Migrate(app,db)
manager = Manager(app)
manager.add_command('db',MigrateCommand)

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

def allowed_image(filename):
    if not "." in filename:
        return False

    ext = filename.rsplit(".", 1)[1]
    if ext.upper() in app.config["ALLOWED_IMAGE_EXTENSIONS"]:
        return True
    else:
        return False

def allowed_image_filesize(filesize):

    if int(filesize) <= app.config["MAX_IMAGE_FILESIZE"]:
        return True
    else:
        return False


## Add users

## Admin Khadim
id_kha = 1
nom_kha = "Khadim NGING"
telephone_kha = "771846968"
adresse_kha = "Camberen"
pass_word_kha = "khadim@poisson2020"
pass_wordx_kha = generate_password_hash(pass_word_kha)


## Admin SARR
id_sar = 2
nom_sar = "Mamadou SARR"
telephone_sar = "777886847"
adresse_sar = "Guédiawaye"
pass_word = "mamadou@poisson2020"
pass_wordx_sar = generate_password_hash(pass_word)

## Admin Ndiaga
id_ndia = 3
nom_ndia = "Ndiaga GAYE"
telephone_ndia = "771817731"
adresse_ndia = "Thies"
pass_word_ndia = "ndiaga@poisson2020"
pass_wordx_ndia = generate_password_hash(pass_word_ndia)


exits_kha = Sessionalchemy.query(User.id).filter(User.telephone==telephone_kha).scalar()
exits_sar = Sessionalchemy.query(User.id).filter(User.telephone==telephone_sar).scalar()
exits_ndia = Sessionalchemy.query(User.id).filter(User.telephone==telephone_ndia).scalar()

if exits_kha == None and exits_sar == None and exits_ndia == None:
    use_kha = User(nom_kha, telephone_kha, pass_wordx_kha)
    use_sar = User(nom_sar,  telephone_sar, pass_wordx_sar)
    use_ndia = User(nom_ndia,  telephone_ndia, pass_wordx_ndia)

    Sessionalchemy.add(use_kha)
    Sessionalchemy.add(use_sar)
    Sessionalchemy.add(use_ndia)
    Sessionalchemy.commit()



################################### LOGIN #################################

#### code pour generer un secret key: python -c 'import os; print(os.urandom(16))'

app.secret_key = "b\x062AA\xe5A\xa7?\t*%\x1a'9\x9f\xd2"

idd = []


@app.route('/login/', methods=['GET', 'POST'])
def login():
    count=0
    # Output message if something goes wrong...
    msg = ''
    # Check if "username" and "password" POST requests exist (user submitted form)
    if request.method == 'POST' and 'telephon' in request.form and 'password' in request.form :
      # Create variables for easy access
        telephon = request.form['telephon']
        pass_wordd = request.form['password']

        hash = []
        exits = Sessionalchemy.query(User.id).filter(User.telephone==telephon).scalar()
        if exits != None:
            data = Sessionalchemy.query(User)\
                .filter(User.telephone == telephon)\
                .all()
            
            for i in data:
                pass_word = i.pass_word
                hash.append(pass_word)
            # pass_words = session.get(pass_word, None)
            val1 = check_password_hash(hash[0], pass_wordd)
            if val1 == True:
            
                # etat_pwd = request.form['etat_pwd']

                person = Sessionalchemy.query(User)\
                    .filter(User.telephone == telephon)\
                    .filter(User.pass_word == hash[0])\
                    .all()
                
                account = []               
                if (telephon != 771846968  and pass_wordd != 'khadim@poisson2020') and (telephon != 777886847  and pass_wordd != 'mamadou@poisson2020') and (telephon != 771817731  and pass_wordd != 'ndiaga@poisson2020'):
                    return redirect(url_for('login'))
                    
                else:
                    for i in person:
                        id = i.id
                        nom = i.nom
                        telephone = i.telephone
                        account.append(id)
                        account.append(nom)
                        account.append(telephone)
                    
                    session['loggedin'] = True
                    session['id'] = account[0] 
                    session['telephon'] = account[2]
                    return redirect(url_for('chef'))
                    
            else:
                flash("Le mot de pass tapé est incorrect!", "danger")
        else:
            flash("Le numéro téléphone est incorrect!", "danger")                        
    return render_template('login.html')


####################################### PARTIE CHEF ###############################

@app.route('/chef', methods=['GET', 'POST'])
def chef():
    count=0
    liste = []
    new_commandes = []
    if session.get('id')==1 or session.get('id')==2 or session.get('id')==3:
        new_commande = []
        comm = Sessionalchemy.query(Commande)\
            .distinct()\
            .count()
        commnonapp = Sessionalchemy.query(Commande)\
            .filter(Commande.appeller=='non')\
            .distinct()\
            .count()
        commnonliv = Sessionalchemy.query(Commande)\
            .filter(Commande.livrer=='non')\
            .distinct()\
            .count()

        commande = Sessionalchemy.query(Commande)\
            .filter(or_(Commande.appeller=='non', Commande.livrer=='non'))\
            .all()

        for i in commande:
            id = i.id
            nom = i.nom
            photo = i.photo
            prix_unitaire = i.prix_unitaire
            quantite = i.quantite
            prix_total = i.prix_total
            appeller = i.appeller
            livrer = i.livrer
            nom_client = i.nom_client
            telephone = i.num_client
            adresse = i.adresse_client
            new_commande.append((id, photo, nom, prix_unitaire, quantite, telephone, adresse, prix_total, appeller, livrer, nom_client))

        
        if len(new_commande) != 0:
            new_commandes = new_commande
        else:
            return redirect(url_for('admin'))
    
    else:
        return redirect(url_for('login')) 

    return render_template('user.html', new_commandes=new_commandes, comm=comm, commnonapp=commnonapp, commnonliv=commnonliv)

# page administrateurs avec 0 modéle invalide
@app.route('/admin', methods=['GET', 'POST'])
def admin():   
    if session.get('id')==1 or session.get('id')==2 or session.get('id')==3:
        comm = Sessionalchemy.query(Commande)\
            .distinct()\
            .count()
        commnonapp = Sessionalchemy.query(Commande)\
            .filter(Commande.appeller=='non')\
            .distinct()\
            .count()
        commnonliv = Sessionalchemy.query(Commande)\
            .filter(Commande.livrer=='non')\
            .distinct()\
            .count()
    else:
        return redirect(url_for('login')) 
    return render_template('admin.html', comm=comm, commnonapp=commnonapp, commnonliv=commnonliv)

@app.route('/liste', methods=['GET', 'POST'])
def liste():   
    if session.get('id')==1 or session.get('id')==2 or session.get('id')==3:
        prod = []
        prods = []
        djeun = Sessionalchemy.query(Djeun)\
            .all()

        for i in djeun:
            idd = i.id
            photo = i.photo
            nom = i.nom
            prix = i.prix

            prod.append((idd, photo,nom,prix))
        if len(prod) != 0:
            prods = prod
        else:
            return redirect(url_for('listevide'))

            
    else:
        return redirect(url_for('login')) 
    return render_template('liste.html', prods=prods)

@app.route('/listevide', methods=['GET', 'POST'])
def listevide():   
    if session.get('id')==1 or session.get('id')==2 or session.get('id')==3:
        comm = Sessionalchemy.query(Commande)\
            .distinct()\
            .count()
    else:
        return redirect(url_for('login')) 
    return render_template('listevide.html')

@app.route('/ajoutprod', methods=['GET', 'POST'])
def ajoutprod():   
    if session.get('id')==1 or session.get('id')==2 or session.get('id')==3:
        nom = request.form.get("nameq")
        prix = request.form.get("prixq")       
        images = request.files.getlist("imageq")
        if len(images)>1:
            flash("Vous avez selectionnez plus d'une image, revoyez svp la saisie.","danger")
            
        else:
            for image in images:
                if image.filename == "":
                    flash("Entrez svp une image", "avertissement")
                    return redirect(request.url)

                if allowed_image(image.filename):
                    filename = secure_filename(image.filename)
                    filenam, file_extension = os.path.splitext(filename)
                    hash = generate_password_hash(filenam)
                    hash=hash[-10:]
                    d = hash+file_extension
                    image.save(os.path.join(app.config["IMAGE_UPLOADS"], d))
                    djeunn = Djeun(nom, d, prix)
                    Sessionalchemy.add(djeunn)
                    Sessionalchemy.commit()
                
                    flash("Un nouveau produit est ajouté sur la liste.","succes")   
                else:
                    return "onk"
    else:
        return redirect(url_for('login')) 
    return render_template('ajoutprod.html')




@app.route('/valider/<string:id_ap>', methods=['GET'])
def valider(id_ap):
    session['modifid'] = id_ap
    modif = []
    admin = Sessionalchemy.query(Djeun)\
        .filter(Djeun.id==id_ap)
    for i in admin:
        photo = i.photo
        nom = i.nom
        prix = i.prix
        modif.append((photo,nom,prix))
    
    # admin.user_id = ids
    Sessionalchemy.commit()
    
    # return redirect(url_for('modif'))
    return render_template('modif.html', modif=modif)

@app.route('/modif', methods=['GET', 'POST'])
def modif():
    if request.method == 'POST':
        ids = session.get('modifid')
        nom = request.form.get("nameq")
        prix = request.form.get("prixq")
        val = Sessionalchemy.query(Djeun)\
            .filter(Djeun.id==ids).first()
        val.nom = nom
        val.prix = prix
        Sessionalchemy.commit()
        flash("Le produit est modifié avec succé.","succes")
    return redirect(url_for('chef'))

@app.route('/supprimer/<string:id_ap>', methods=['GET'])
def supprimer(id_ap):
    phot=[]
    val =Sessionalchemy.query(Djeun)\
        .filter(Djeun.id==id_ap)\
            .all()
    for i in val:
        photo = i.photo
        phot.append(photo)
    print(phot)
    Sessionalchemy.query(Djeun)\
        .filter(Djeun.id==id_ap).delete()
    Sessionalchemy.commit()
    
    os.remove(os.path.join(app.config["IMAGE_UPLOADS"], phot[0]))

    flash("Le produit est supprimé de la liste.","danger")

    return redirect(url_for('chef'))


# Page Accueil

@app.route('/')
# # @app.cache.cached(timeout=3600)     # mettre en cache chaque 1h
def accueil():
    ret = "yes accueil"
    page = int(request.args.get('page', 1))
    per_page = 20
    offset = (page - 1) * per_page

    vendeur=[]
    id_user = session.get('id',None)

    djeuns = Sessionalchemy.query(Djeun)\
        .all()
    djeun = []
    for i in djeuns:   
        photo = i.photo
        nom = i.nom
        prix = i.prix
        id = i.id
        djeun.append((photo, nom, prix, id))
                                       
    total = len(djeun)
    paginationUsers = djeun[offset: offset + per_page]
    pagination = Pagination(page=page, per_page=per_page, total=total,
                            css_framework='bootstrap4')
  
    return render_template('accueil.html', users=paginationUsers, page=page, per_page=per_page,pagination=pagination)


#########################################  DECONNEXION  ###########################

@app.route('/deconnexion')
def logout():
    # Remove session data, this will log the user out
   session.pop('loggedin', None)
   session.pop('id', None)
   session.pop('nom_utilisateur', None)
   # Redirect to login page
   return redirect(url_for('login'))

   return render_template('deconnexion.html')

if __name__ == '__main__': #si le fichier est executer alors execute le bloc
    app.run(debug=True) #debug=True relance le serveur à chaque modification

