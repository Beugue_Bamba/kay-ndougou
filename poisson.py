
# coding=utf-8
#####################################################################################"
# import flask_login
# encoding=utf8
# import sys
# reload(sys)
# sys.setdefaultencoding('utf8')

from collections import Counter
import tempfile
import pickle          
import shutil
from flask import Flask, render_template, redirect, url_for, request, flash, session, g,jsonify
import psycopg2
import datetime
import re  # verification des mails, noms, etc
from werkzeug.security import generate_password_hash
from werkzeug.security import check_password_hash, check_password_hash
import datetime
from PIL import Image
import json 
# from resizeimage import resizeimage                

import os
from werkzeug.utils import secure_filename
import pandas as pd
import numpy as np
from flask_paginate import Pagination, get_page_args
from flask_sqlalchemy import SQLAlchemy

from flask_cors import CORS
from sqlalchemy import or_, cast, String, desc
# from flask_caching import Cache




# 1 - imports tables of data base
from datetime import date

from basepoissons import Sessionalchemy, engine, Base
from produits import Produits
from commandes import Commandes
from users import User
from djeun import Djeun
from message import Message
from liste import li
from depenses import Depenses
from boutiques import Boutiques
from stockage import Stockages


# 2 - generer le shema la base de données
Base.metadata.create_all(engine)
######################################FLASK MIGRATE###################################
# from flask_script import Manager
# from flask_migrate import Migrate, MigrateCommand
#######################################################################################

SQLALCHEMY_TRACK_MODIFICATIONS = False

###################### CREATION DE MOT DE PASS ALEATOIRE ##################
import string
from random import sample

app = Flask(__name__)
CORS(app)
app.secret_key = "b\x062AA\xe5A\xa7?\t*%\x1a'9\x9f\xd2"



# define the cache config keys, remember that it can be done in a settings file
app.config['CACHE_TYPE'] = 'simple'
# register the cache instance and binds it on to your app 
# app.cache = Cache(app) 

# HOSTNAME = '127.0.0.1'
# PORT = '5432'
# DATABASE = 'dbpoissons'
# USERNAME = 'postgres'
# PASSWORD = 'Kh@dim1991'




app.config["IMAGE_UPLOADS"] ="static/images"
app.config["ALLOWED_IMAGE_EXTENSIONS"] = ["JPEG", "JPG", "PNG", "GIF", "tiff", "HEIF", "HEIC"]
ALLOWED_EXTENSIONS = set([ 'png', 'jpg', 'jpeg', 'gif', 'tiff', 'heif', 'heic'])

app.config["MAX_IMAGE_FILESIZE"] = 0.5 * 4050 * 4050
# app.config['SQLALCHEMY_DATABASE_URI']="postgres+psycopg2://{}:{}@{}:{}/{}".format (USERNAME,PASSWORD,HOSTNAME,PORT,DATABASE)
Sessionalchemy = Sessionalchemy()

db = SQLAlchemy(app)

# migrate = Migrate(app,db)
# manager = Manager(app)
# manager.add_command('db',MigrateCommand)

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

def allowed_image(filename):
    if not "." in filename:
        return False

    ext = filename.rsplit(".", 1)[1]
    if ext.upper() in app.config["ALLOWED_IMAGE_EXTENSIONS"]:
        return True
    else:
        return False

def allowed_image_filesize(filesize):

    if int(filesize) <= app.config["MAX_IMAGE_FILESIZE"]:
        return True
    else:
        return False


## Add users

## Admin Khadim
id_kha = 1
prenom_kha = "Khadim"
nom_kha = "NGING"
telephone_kha = "771846968"
mail_kha = "khadim@gmail.com"
adresse_kha = "Camberen"
region = ""
code = ""
cni_kha = "000"
pass_word_kha = "khadim@poisson2020"
pass_wordx_kha = generate_password_hash(pass_word_kha)
datenaiss_kha = "14/11/1993"
role_kha = "admin"
etat_password = True


## Admin SARR
id_sar = 2
prenom_sar = "Mamadou"
nom_sar = "SARR"
telephone_sar = "777886847"
mail_sar = "mamadoutive@yahoo.fr"
adresse_sar = "Guédiawaye"
pass_word = "mamadou@poisson2020"
pass_wordx_sar = generate_password_hash(pass_word)
cni_sar = "1633200300126"
datenaiss_sar = "17/11/1994"
role_sar = "admin"
etat_password = True

## Admin Ndiaga
id_ndia = 3
prenom_ndia = "Ndiaga"
nom_ndia = "GAYE"
telephone_ndia = "771817731"
mail_ndia = "ndiaga@gmail.com"
adresse_ndia = "Thies"
pass_word_ndia = "ndiaga@poisson2020"
pass_wordx_ndia = generate_password_hash(pass_word_ndia)
cni_ndia = "0000"
datenaiss_ndia = "12/09/1991"
role_ndia = "admin"
etat_password = True


## Comptable
id_compt = 4
prenom_compt = "Comptable"
nom_compt = "Compatble"
telephone_compt = "773333333"
mail_compt = "comptable@gmail.com"
adresse_compt = "Dakar"
pass_word_compt = "comptable@senndawal2020"
pass_wordx_compt = generate_password_hash(pass_word_compt)
cni_compt = "111"
datenaiss_compt = "03/03/2003"
role_compt = "comptable"
etat_password = True

## Agent markting
id_ba = 4
prenom_ba = "Baba"
nom_ba = "MAL"
telephone_ba = "777886848"
mail_ba = "mamadoutive@yahoo.fr"
adresse_ba = "Guediawaye"
pass_word_ba = "markting@senndawal2020"
pass_wordx_ba = generate_password_hash(pass_word_ba)
cni_ba = "444"
datenaiss_ba = "05/05/2015"
role_ba = "markting"
etat_password = True
statut = "actif"
pourcentage = ""
montant = ""
salaire = ""
conseil_admin = "oui"
boutschef = ""
boutique = ""
# boutsq = Sessionalchemy.query(User.id)\
#     .filter(User.nomboutique==boutique)\
#     .one()
idsboutchef = Sessionalchemy.query(Boutiques).filter_by(nomboutique=boutique).first()

exits_kha = Sessionalchemy.query(User.id).filter(User.telephone==telephone_kha).scalar()
# exits_sar = Sessionalchemy.query(User.id).filter(User.telephone==telephone_sar).scalar()
exits_ndia = Sessionalchemy.query(User.id).filter(User.telephone==telephone_ndia).scalar()
exits_compt = Sessionalchemy.query(User.id).filter(User.telephone==telephone_compt).scalar()

if exits_kha == None and exits_ndia == None:
    use_kha = User(prenom_kha, nom_kha, telephone_kha, mail_kha, pass_wordx_kha, adresse_kha, region, code, cni_kha, datenaiss_kha, role_kha, etat_password, statut, pourcentage, montant, salaire, conseil_admin, idsboutchef)
    use_sar = User(prenom_sar, nom_sar,  telephone_sar, mail_sar, pass_wordx_sar, adresse_sar, region, code, cni_sar, datenaiss_sar, role_sar, etat_password, statut, pourcentage, montant, salaire, conseil_admin, idsboutchef)
    use_ndia = User(prenom_ndia, nom_ndia,  telephone_ndia, mail_ndia, pass_wordx_ndia, adresse_ndia, region, code, cni_ndia, datenaiss_ndia, role_ndia, etat_password, statut, pourcentage, montant, salaire, conseil_admin, idsboutchef)
    use_ba = User(prenom_ba, nom_ba, telephone_ba, mail_ba, pass_wordx_ba, adresse_ba, region, code, cni_ba, datenaiss_ba, role_ba, etat_password, statut, pourcentage, montant, salaire, conseil_admin, idsboutchef)
    


    Sessionalchemy.add(use_kha)
    Sessionalchemy.add(use_sar)
    Sessionalchemy.add(use_ndia)
    Sessionalchemy.add(use_ba)
    Sessionalchemy.commit()

if exits_compt == None:
    use_compt = User(prenom_compt, nom_compt, telephone_compt, mail_compt, pass_wordx_compt, adresse_compt, region, code, cni_compt, datenaiss_compt, role_compt, etat_password, statut, pourcentage, montant, salaire, conseil_admin, idsboutchef)
    Sessionalchemy.add(use_compt)
    Sessionalchemy.commit()

#################################  Insertion de trois produits 
#poisson

idpois = 1
nompos = "Dorade Moyen"
phopois = "dorad_bu_mak.png"
prixpois = 2700
oldpois = 2900
typespois = "poissons"
decripois = "Chaque piéce de ce poisson pése environ 500g.Le poisson dorade constitue une bonne source de vitamines du groupe B, notamment B12. Il apporte également de la vitamine E anti-oxydante"
valpoi = "kg"

nompossq = "Dorade Yeux jaunes"
phopoissq = "bc4b7eaf65.png"
prixpoissq = 2400
oldpoissq = 2600
typespoissq = "poissons"
decripoissq = "Le poisson dorade constitue une bonne source de vitamines du groupe B, notamment B12. Il apporte également de la vitamine E anti-oxydante"
valpoisq = "kg"

nomposdd = "Dorade Petit"
phopoisdd = "DORADE-ROSE.png"
prixpoisdd = 2500
oldpoisdd = 2700
typespoisdd = "poissons"
decripoisdd = "Chaque piéce de ce poisson pése environ 300g.Le poisson dorade constitue une bonne source de vitamines du groupe B, notamment B12. Il apporte également de la vitamine E anti-oxydante"
valpoidd = "kg"

nomposddd = "Dorade Grand"
phopoisddd = "tout-savoir-sur-la-dorade.jpeg"
prixpoisddd = 3300
oldpoisddd = 3700
typespoisddd = "poissons"
decripoisddd = "Chaque piéce de ce poisson pése environ 1kg et +.Le poisson dorade constitue une bonne source de vitamines du groupe B, notamment B12. Il apporte également de la vitamine E anti-oxydante"
valpoiddd = "kg"

nomposdddd = "Guisse"
phopoisdddd = "guiss.jpg"
prixpoisdddd = 2500
oldpoisdddd = 2800
typespoisdddd = "poissons"
decripoisdddd = "Guisse, un bon poisson riche en proteine"
valpoidddd = "kg"

## poulet
idpoulet = 2
nompoulet = "Poulet de 2kg et +"
phopoulet = "a2117775de.png"
prixpoulet = 3500
oldpoulet = 3700
typespoulet = "poulets"
decripoulet =  "Votre meilleur poulet entier à un prix imbattable"
valpoulet = "ut"


## Viande mouton
idmouton = 3     
nommouton = "Viande mouton"
phomouton = "moutonimg7.jpg"
prixmouton = 3200     
oldmouton = 3500
typesmouton = "viandemouton" 
decrimouton =  "Viande mouton"
valmouton = "kg"

## Viande Boeuf
idboeuf = 4
nomboeuf = "Viande boeuf"
phoboeuf = "boeufimg3.png"
prixboeuf = 3000
oldboeuf = 3200
typesboeuf = "viandeboeuf"
decriboeuf =  "Viande bœuf"    
valboeuf = "kg"


# ##################### viandes moutons 

a1 = "Côtelette(farr) mouton"
a2 = "cottelettemouton.jpg"
a3 = 3200
a4 = 3500
a5 = "viandemouton"
a6 =  "La côtelette est une côte de mouton ou agneau appelée en wolof faarou xar"
a7 = "kg"



b1 = "Epaule mouton"
b2 = "epaule.jpg"
b3 =3200
b4 = 3500
b5 ="viandemouton"
b6 ="Épaule de mouton"
b7 = "kg"

b1b = "Agneau entier"
b2b = "agneau-entier2.jpg"
b3b = 3500
b4b = 3800
b5b ="viandemouton"
b6b ="Agnon entier environ 8kg"
b7b = "kg"

c1 = "Foie mouton"
c2 ="foie(ress).jpg"
c3 = 3000
c4 = 3500
c5 ="viandemouton"
c6 = "Foie de mouton appelée en wolof  ressou xar"
c7 = "ut"

d1 = "Gigot mouton"
d2 = "gigot.jpg"
d3 = 3200
d4 = 3500
d5 = "viandemouton"
d6 = "Gigot de mouton appelé en wolof podiou xar"
d7 = "kg"

e1 = "Pied de mouton"
e2 = "Pieddemouton(yelouxar).jpg"
e3 = 200
e4 = 250
e5 = "viandemouton"
e6 = "Pied de mouton appelé en wolof yelou xar"
e7 = "ut"

f1 = "Mouton de 13 kg"
f2 = "agneau-entier_s.jpg"  
f3 = 50000
f4 = 56000
f5 = "viandemouton"
f6 = "Mouton entier de 12 à 13 kg"
f7 = "ut"

g1 = "Mouton entier"  
g2 = "mouton18.png"
g3 = 35000
g4 = 40000
g5 = "viandemouton"
g6 = "Mouton entier environ 10kg"
g7 = "ut"

g1g = "Mouton entier"  
g2g= "agneau-entier_s.jpg"
g3g = 50000
g4g = 55000
g5g = "viandemouton"
g6g = "Mouton entier environ 15kg"
g7g = "ut"

h1 = "Carcasse de mouton de 13 kg"
h2 = "carcassemouton.jpg"
h3 = 50000
h4 = 56000
h5 = "viandemouton"
h6 = "Carcasse de mouton de 12 à 13 kg"
h7 = "ut"

i1 = "Agneau 8 kg"
i2 = "agneau1.jpg"
i3 = 28500
i4 = 30000
i5 = "viandemouton"
i6 = "Viande agneau de 8 kg, de la tête au pied, nous vous assurons une meilleure qualité de viande"
i7 = "ut"

j1 = "Agneau 9 kg"
j2 = "agneau2.jpg"
j3 = 31500
j4 = 35000
j5 = "viandemouton"
j6 = "Viande agneau de 9 kg, de la tête au pied, nous vous assurons une meilleure qualité de viande"
j7 = "ut"

k1 = "Agneau 10 kg"
k2 = "agneau1.jpg"
k3 = 35000
k4 = 39000
k5 = "viandemouton"
k6 = "Viande agneau de 10 kg, de la tête au pied, nous vous assurons une meilleure qualité de viande"
k7 = "ut"

l1 = "Agneau 12 kg"
l2 = "agneau2.jpg"
l3 = 41500
l4 = 45000
l5 = "viandemouton"
l6 = "Viande agneau de 12 kg, de la tête au pied, nous vous assurons une meilleure qualité de viande"
l7 = "ut"





###############  viande boeuf 

aa1 = "Abats boeuf"
aa2 = "279_1.png"
aa3 = 1400
aa4 = 1500
aa5 = "viandeboeuf"
aa6 = "Abats de boeuf est constitué de la tête, du ventre et de la queue du  boeuf"
aa7 = "kg"

bb1 = "Colis de bœuf"
bb2 = "Colisdebœuf.png"
bb3 = 3000
bb4 = 3200
bb5 = "viandeboeuf"
bb6 = "Viande de bœuf de très bonne qualité"
bb7 = "kg"

cc1 = "Côtelette boeuf"
cc2 = "côtelette(faar).jpg"
cc3 = 3000
cc4 = 3200
cc5 = "viandeboeuf"
cc6 = "La côtelette est une côte de boeuf appelée en wolof faarou nakk"
cc7 = "kg"

dd1 = "Epaule boeuf"
dd2 = "epauleboeuf.jpg"
dd3 = 3000
dd4 = 3200
dd5 = "viandeboeuf"
dd6 = "Epaule de bœuf"
dd7 = "kg"

ee1 = "Faux filet boeuf"
ee2 = "Fauxfiletboeuf.jpg"
ee3 = 3000
ee4 = 3200
ee5 = "viandeboeuf"
ee6 = "Faux filet de boeuf"
ee7 = "kg"

ff1 = "Faux filet boeuf"
ff2 = "Fauxfilet.jpg"
ff3 = 3000
ff4 = 3200
ff5 = "viandeboeuf"
ff6 = "Faux filet de boeuf"
ff7 = "kg"

gg1 = "Foie de boeuf"
gg2 = "foieboeuf.jpg"
gg3 = 3000
gg4 = 3200
gg5 = "viandeboeuf"
gg6 = "Foie de bœuf appelé en wolof ressou nakk"
gg7 = "kg"

hh1 = "Foie de boeuf"
hh2 = "foieboeuf1.png"
hh3 = 3000
hh4 = 3200
hh5 = "viandeboeuf"
hh6 = "Foie de bœuf appelé en wolof ressou nakk"
hh7 = "kg"

ii1 = "Queue de boeuf"
ii2 = "queue2.jpg"
ii3 = 2900
ii4 = 3000
ii5 = "viandeboeuf"
ii6 = "Queue de bœuf appelé en wolof thialguéne"
ii7 = "kg"

jj1 = "Pied de boeuf"
jj2 = "piedboeuf.jpg"
jj3 = 2000
jj4 = 2200
jj5 = "viandeboeuf"
jj6 = "Pied de bœuf appelé en wolof yellou nakk"
jj7 = "ut"

kk1 = "Viande sans os de boeuf"
kk2 = "Viandesansos.jpg"
kk3 = 3800
kk4 = 4000
kk5 = "viandeboeuf"
kk6 = "viande de boeuf sans os"
kk7 = "kg"


ll1 = "Laxass"
ll2 = "laxass1.jpg"
ll3 = 100
ll4 = 125
ll5 = "viandeboeuf"
ll6 = "laxass"
ll7 = "ut"


mm1 = "Filet de boeuf"
mm2 = "filet_boeuf.png"
mm3 = 6000
mm4 = 6300
mm5 = "viandeboeuf"
mm6 = "Filet de boeuf"
mm7 = "kg"

nn1 = "Jarret boeuf"
nn2 = "jarrat_boeuf.jpg"
nn3 = 3000
nn4 = 3200
nn5 = "viandeboeuf"
nn6 = "Jarret boeuf"
nn7 = "kg"



########### poisson

aaa1 = "Sardinelle(Yaboy)"
aaa2 = "sardinelle.jpg"
aaa4 = 1500
aaa3 = 1300
aaa5 = "poissons"
aaa6 = "La sardinelle constitue une excellente source de vitamines, elle fournit des protéines et des lipides."
aaa7 = "kg"


bbb1 = "Diey"
bbb2 = "dieyvrai.jpg"
bbb4 = 3000
bbb3 = 2700
bbb5 = "poissons"
bbb6 = "diéy est un poisson fusiforme allongé au corps recouvert d’écailles très fines et très étroite mais de grande extension verticale."
bbb7 = "kg"

ccc1 = "Barracuda(Seudde)"
ccc2 = "barracuda.jpg"
ccc4 = 4000
ccc3 = 3800
ccc5 = "poissons"
ccc6 = "Le barracuda fait partie des prédateurs marins les plus répandus et les plus connus au monde."
ccc7 = "kg"

ddd1 = "Yaakh bu mak (carpe rouge grande)"
ddd2 = "yaakh.png"
ddd4 = 4400
ddd3 = 4000
ddd5 = "poissons"
ddd6 = "En général une piéce de carpe rouge grande pése 10kg et + ."
ddd7 = "kg"

eee1 = "Sompate Grand"
eee2 = "sompateq.png"
eee4 = 3000
eee3 = 3300
eee5 = "poissons"
eee6 = "Chaque piéce de ce poisson pése environ 900g ou +. Sompate est un poisson qui possède un large spectre alimentaire, sa taille moyenne se situe autour de 50-60 cm."
eee7 = "kg"

fff1f = "Sompate Petit"
fff2f = "sompate22.jpg"
fff4f = 2000
fff3f = 2200
fff5f = "poissons"
fff6f = "Chaque piéce de ce poisson pése environ 300g ou +. Sompate est un poisson qui possède un large spectre alimentaire, sa taille moyenne se situe autour de 50-60 cm."
fff7f = "kg"

ggg1 = "Todjé"
ggg2 = "todje.png"
ggg4 = 3300
ggg3 = 3000
ggg5 = "poissons"
ggg6 = "todjé est un poisson riche en calories, en protéines et en glucides"
ggg7 = "kg"


hhh1h = "Capitaine au four"
hhh2h = "capitaine_au_four.jpg"
hhh4h = 3000
hhh3h = 2700
hhh5h = "poissons"
hhh6h = "Une piéce de capitaine au four pése environ 300g à 500g. Le poisson capitaine représente une source de protéines de qualité et de matières grasses bénéfiques à la santé."
hhh7h = "kg"

hhh1hh = "Capitaine G"
hhh2hh = "capitaine_g.png"
hhh4hh = 4000
hhh3hh = 3700
hhh5hh = "poissons"
hhh6hh = "Une piéce de capitaine G pése environ 1kg et +. Le poisson capitaine représente une source de protéines de qualité et de matières grasses bénéfiques à la santé."
hhh7hh = "kg"

hhh1hhh = "Rouget"
hhh2hhh = "rouget.jpg"
hhh4hhh = 2500
hhh3hhh = 2200
hhh5hhh = "poissons"
hhh6hhh = "Le poisson rouget est trés riche en protéines de qualité et de matières grasses bénéfiques à la santé."
hhh7hhh = "kg"



hhh1 = "Thiof G"
hhh2 = "thiofg.jpg"
hhh4 = 4200
hhh3 = 4000
hhh5 = "poissons"
hhh6 = "Elle représente une source de protéines de qualité et de matières grasses bénéfiques à la santé."
hhh7 = "kg"


iii1 = "Thon blanc"
iii2 = "Thonblanc.png"
iii4 = 2600
iii3 = 2400
iii5 = "poissons"
iii6 = "Le thon est une excellente source de protéines."
iii7 = "kg"

jjj1 = "Niaw nekh"
jjj2 = "niawnekh1.png"
jjj4 = 2700
jjj3 = 2500
jjj5 = "poissons"
jjj6 = "c'est un poisson riche au matières grasses."
jjj7 = "kg"

kkk1 = "Niaw nekh"
kkk2 = "niawnekh2.png"
kkk4 = 2700
kkk3 = 2500
kkk5 = "poissons"
kkk6 = "c'est un poisson riche au matières grasses."
kkk7 = "kg"

lll1 = "Crevette"
lll2 = "CREVETTE.jpg"
lll4 = 6300
lll3 = 4500
lll5 = "poissons"
lll6 = "la crevette  rose possède une excellente valeur nutritive"
lll7 = "kg"

mmm1 = "Diéregne"
mmm2 = "sz.jpg"
mmm4 = 2500
mmm3 = 2100
mmm5 = "poissons"
mmm6 = "Ce poisson constitue une bonne source de vitamines du groupe B, notamment B12."
mmm7 = "kg"

nnn1 = "Filet de dorade"
nnn2 = "FiletDorade.png"
nnn4 = 7200
nnn3 = 6900
nnn5 = "poissons"
nnn6 = "poisson Daurade mis en filet"
nnn7 = "kg"

ooo1 = "Bande"
ooo2 = "bande.png"
ooo4 = 2000
ooo3 = 1800
ooo5 = "poissons"
ooo6 = "Son profil nutritionnel se distingue par un excellent apport en protéines de bonne qualité"
ooo7 = "kg"

ppp1 = "Lotte"
ppp2 = "lotte.jpg"
ppp4 = 2200
ppp3 = 2000
ppp5 = "poissons"
ppp6 = "Son profil nutritionnel se distingue par un excellent apport en protéines de bonne qualité"
ppp7 = "kg"

ppp1pp = "Filet de Lotte"
ppp2pp = "filet_lotte.png"
ppp4pp = 4300
ppp3pp = 4000
ppp5pp = "poissons"
ppp6pp = "Son profil nutritionnel se distingue par un excellent apport en protéines de bonne qualité"
ppp7pp = "kg"


ppp1p = "Lotte brutte"
ppp2p = "lotte_brute.jpg"
ppp4p = 1200
ppp3p = 1000
ppp5p = "poissons"
ppp6p = "Son profil nutritionnel se distingue par un excellent apport en protéines de bonne qualité"
ppp7p = "kg"


qqq1 = "Badeche (doye)"
qqq2 = "badeche-rouge.png"
qqq4 = 4000
qqq3 = 3800
qqq5 = "poissons"
qqq6 = "Son profil nutritionnel se distingue par un excellent apport en protéines de bonne qualité"
qqq7 = "kg"

rrr1 = "Thiof au four"
rrr2 = "thiof_au_four.jpg"
rrr4 = 4700
rrr3 = 4500
rrr5 = "poissons"
rrr6 = "Son profil nutritionnel se distingue par un excellent apport en protéines de bonne qualité"
rrr7 = "kg"

sss1 = "Sole"
sss2 = "soletigre.jpg"
sss4 = 2200
sss3 = 2000
sss5 = "poissons"
sss6 = "Son profil nutritionnel se distingue par un excellent apport en protéines de bonne qualité"
sss7 = "kg"

ttt1 = "Badéche (doye)"
ttt2 = "badeche-rouge.png"
ttt4 = 4000
ttt3 = 3800
ttt5 = "poissons"
ttt6 = "Son profil nutritionnel se distingue par un excellent apport en protéines de bonne qualité"
ttt7 = "kg"


uuu1 = "Yaakh bu ndaw (carpe rouge moyenne)"   
uuu2 = "yaakh.png"
uuu4 = 4800
uuu3 = 4500
uuu5 = "poissons"
uuu6 = "Une piéce de carpe rouge moyenne pése entre 1kg à 9kg."
uuu7 = "kg"

######### poulet

aaaa1 = "Poulet de 1,5kg à 1,6kg"
aaaa2 = "poulet2kg.jpg"
aaaa4 = 3000
aaaa3 = 2750
aaaa5 = "poulets"
aaaa6 = "poulet entier"
aaaa7 = "ut" 


bbbb1 = "Poulet de 1,7kg à 1,9kg"
bbbb2 = "poulet25.jpg"
bbbb4 = 3200
bbbb3 = 3000
bbbb5 = "poulets"
bbbb6 = "poulet entier "
bbbb7 = "ut"

bbbb1b = "Plateaux Cuisse de poulets"
bbbb2b = "cuisse_poulet.jpg"
bbbb4b = 2500
bbbb3b = 2200
bbbb5b = "poulets"
bbbb6b = "Le plateaux de cuisse de poulets pése environ 1kg "
bbbb7b = "ut"

bbbb1bb = "Blanc de poulets"
bbbb2bb = "blanc-de-poulet.jpg"
bbbb4bb = 4000
bbbb3bb = 3800
bbbb5bb = "poulets"
bbbb6bb = "Blanc de poulet"
bbbb7bb = "kg"

bbbb1bbb = "Ailes de poulets"
bbbb2bbb = "aile-poulet.jpg"
bbbb4bbb = 1800
bbbb3bbb = 1500
bbbb5bbb = "poulets"
bbbb6bbb = "Ailes de poulet"
bbbb7bbb = "kg"


cccc1 = "Poulet 1,7kg à 1,9kg"
cccc2 = "dd.jpg"
cccc4 = 3200
cccc3 = 3000
cccc5 = "poulets"
cccc6 = "poulet entier "
cccc7 = "ut"

dddd1 = "Poulet inferieur à 1,5kg"
dddd2 = "ss.jpg"
dddd4 = 2700
dddd3 = 2500
dddd5 = "poulets"
dddd6 = "poulet entier "
dddd7 = "ut"


#################   DINDE
aaaaa1 = "dinde entiére"
aaaaa2 = "dinde1.jpg"
aaaaa4 = 4500
aaaaa3 = 4250
aaaaa5 = "dindes"
aaaaa6 = "Viande de dinde de trés bonne qualité "
aaaaa7 = "kg"

bbbbb1 = "dinde entiére"
bbbbb2 = "dinde2.png"
bbbbb4 = 4500
bbbbb3 = 4250
bbbbb5 = "dindes"
bbbbb6 = "Viande de dinde de trés bonne qualité "
bbbbb7 = "kg"

ccccc1 = "Viande de dinde"
ccccc2 = "dinde3.png"
ccccc4 = 4500
ccccc3 = 4250
ccccc5 = "dindes"
ccccc6 = "Viande de dinde dépiécée de trés bonne qualité "
ccccc7 = "kg"

ddddd1 = "Viande de dinde dépiécée"
ddddd2 = "dinde5.jpg"
ddddd4 = 4500
ddddd3 = 4250
ddddd5 = "dindes"
ddddd6 = "Viande de dinde dépiécée de trés bonne qualité "
ddddd7 = "kg"

eeeee1 = "Viande de dinde"
eeeee2 = "dinde4.png"
eeeee4 = 4500
eeeee3 = 4250
eeeee5 = "dindes"
eeeee6 = "Viande de dinde de trés bonne qualité "
eeeee7 = "kg"

fffff1 = "Viande de dinde"
fffff2 = "dinde6.png"
fffff4 = 4500
fffff3 = 4250
fffff5 = "dindes"
fffff6 = "Viande de dinde de trés bonne qualité "
fffff7 = "kg"

ggggg1 = "pilons de dinde"
ggggg2 = "pilons-de-dinde.jpg"
ggggg4 = 4500
ggggg3 = 4250
ggggg5 = "dindes"
ggggg6 = "pilons de dinde de trés bonne qualité "
ggggg7 = "kg"


################   PIGEONS 

aaaaaa1 = "pigeons"
aaaaaa2 = "pigeon1.jpg"
aaaaaa4 = 2000
aaaaaa3 = 1900
aaaaaa5 = "pigeons"
aaaaaa6 = "pigeons de trés bonne qualité "
aaaaaa7 = "ut"

bbbbbb1 = "pigeons déjà néttoyés"
bbbbbb2 = "pigeon6.png"
bbbbbb4 = 2200
bbbbbb3 = 2000
bbbbbb5 = "pigeons"
bbbbbb6 = "viandes pigeons de trés bonne qualité "
bbbbbb7 = "ut"

cccccc1 = "pigeons déjà néttoyés"
cccccc2 = "pigeon7.png"
cccccc4 = 2200
cccccc3 = 2000
cccccc5 = "pigeons"
cccccc6 = "viandes pigeons de trés bonne qualité "
cccccc7 = "ut"

dddddd1 = "pigeons"
dddddd2 = "pigeon4.jpg"
dddddd4 = 2000
dddddd3 = 1900
dddddd5 = "pigeons"
dddddd6 = "pigeons de trés bonne qualité "
dddddd7 = "ut"

eeeeee1 = "pigeons"
eeeeee2 = "pigeon3.jpg"
eeeeee4 = 2000
eeeeee3 = 1900
eeeeee5 = "pigeons"
eeeeee6 = "pigeons de trés bonne qualité "
eeeeee7 = "ut"

ffffff1 = "pigeons déjà néttoyés"
ffffff2 = "pigeon8.png"
ffffff4 = 2200
ffffff3 = 2000
ffffff5 = "pigeons"
ffffff6 = "pigeons de trés bonne qualité"
ffffff7 = "ut"

#######################  CREVETTES

aaaaaaa1 = "cameron"
aaaaaaa2 = "camaron14000.jpg"
aaaaaaa4 = 12500
aaaaaaa3 = 12000
aaaaaaa5 = "crevettes"
aaaaaaa6 = "camaron"
aaaaaaa7 = "kg"


bbbbbbb1 = "camaron"
bbbbbbb2 = "camaron-14000.jpg"
bbbbbbb4 = 14500
bbbbbbb3 = 14000
bbbbbbb5 = "crevettes"
bbbbbbb6 = "camaron"
bbbbbbb7 = "kg"


ccccccc1 = "Gambas num:1"
ccccccc2 = "gambass,n:1,11000fr.jpg"
ccccccc4 = 11700
ccccccc3 = 11000
ccccccc5 = "crevettes"
ccccccc6 = "gambas numero 1"
ccccccc7 = "kg"


ddddddd1 = "Crevette"
ddddddd2 = "crevette-3000.jpg"
ddddddd4 = 3300
ddddddd3 = 3000
ddddddd5 = "crevettes"
ddddddd6 = "crevette"
ddddddd7 = "kg"


eeeeeee1 = "Gambas num:2"
eeeeeee2 = "gambass,n:2,9000fr.png"
eeeeeee4 = 9600
eeeeeee3 = 9000
eeeeeee5 = "crevettes"
eeeeeee6 = "gambas numero 2"
eeeeeee7 = "kg"
     

fffffff1 = "Gambas num:3"
fffffff2 = "gambass,n:3,7000fr.jpg"
fffffff4 = 7400
fffffff3 = 7000
fffffff5 = "crevettes"
fffffff6 = "gambas numero 4"
fffffff7 = "kg"

ggggggg1 = "Crevette"
ggggggg2 = "crevettes-4500.jpeg"
ggggggg4 = 4900
ggggggg3 = 4500
ggggggg5 = "crevettes"
ggggggg6 = "Crevette"
ggggggg7 = "kg"


  
  
# exits_poisson = Sessionalchemy.query(Djeun.id).filter(Djeun.nom==nompos).scalar()
exits_poulet = Sessionalchemy.query(Djeun.id).filter(Djeun.nom==nompoulet).scalar()  
#exits_mouton = Sessionalchemy.query(Djeun.id).filter(Djeun.nom==nommouton).scalar()  
# print(exits_poisson, exits_poulet, exits_mouton)
if exits_poulet == None :
    poissondd = Djeun(nomposdd, phopoisdd, prixpoisdd, oldpoisdd, typespoisdd, decripoisdd, valpoidd)
    mouton = Djeun(nommouton, phomouton, prixmouton, oldmouton, typesmouton, decrimouton,valmouton)
    poulet = Djeun(nompoulet, phopoulet, prixpoulet, oldpoulet, typespoulet, decripoulet, valpoulet)
    boeuf = Djeun(nomboeuf, phoboeuf, prixboeuf, oldboeuf, typesboeuf, decriboeuf, valboeuf)

    moutonsa = Djeun(a1, a2, a3, a4, a5, a6, a7)
    pouletsa = Djeun(aaaa1, aaaa2, aaaa3, aaaa4, aaaa5, aaaa6, aaaa7)
    poissonddd = Djeun(nomposddd, phopoisddd, prixpoisddd, oldpoisddd, typespoisddd, decripoisddd, valpoiddd)
    poissondddd = Djeun(nomposdddd, phopoisdddd, prixpoisdddd, oldpoisdddd, typespoisdddd, decripoisdddd, valpoidddd)

    poissonsqs = Djeun(nompossq, phopoissq, prixpoissq, oldpoissq, typespoissq, decripoissq, valpoisq)
    poissonsa = Djeun(aaa1, aaa2, aaa3, aaa4, aaa5, aaa6, aaa7)
    poissonso = Djeun(ooo1, ooo2, ooo3, ooo4, ooo5, ooo6, ooo7)
    poisson = Djeun(nompos, phopois, prixpois, oldpois, typespois, decripois, valpoi)
    poissonsp = Djeun(ppp1, ppp2, ppp3, ppp4, ppp5, ppp6, ppp7)
    poissonsq = Djeun(qqq1, qqq2, qqq3, qqq4, qqq5, qqq6, qqq7)
    poissonsr = Djeun(rrr1, rrr2, rrr3, rrr4, rrr5, rrr6, rrr7)
    poissonss = Djeun(sss1, sss2, sss3, sss4, sss5, sss6, sss7)
    poissonst = Djeun(ttt1, ttt2, ttt3, ttt4, ttt5, ttt6, ttt7)
    poissonspp = Djeun(ppp1p, ppp2p, ppp3p, ppp4p, ppp5p, ppp6p, ppp7p)
    poissonsppp = Djeun(ppp1pp, ppp2pp, ppp3pp, ppp4pp, ppp5pp, ppp6pp, ppp7pp)
    
    poissonsh = Djeun(hhh1, hhh2, hhh3, hhh4, hhh5, hhh6, hhh7)
    poissonshhhh = Djeun(hhh1hhh, hhh2hhh, hhh3hhh, hhh4hhh, hhh5hhh, hhh6hhh, hhh7hhh)

    moutonsg = Djeun(g1, g2, g3, g4, g5, g6, g7)
    moutonsgg = Djeun(g1g, g2g, g3g, g4g, g5g, g6g, g7g)
    poissonshh = Djeun(hhh1h, hhh2h, hhh3h, hhh4h, hhh5h, hhh6h, hhh7h)
    
    creva = Djeun(aaaaaaa1, aaaaaaa2, aaaaaaa3, aaaaaaa4, aaaaaaa5, aaaaaaa6, aaaaaaa7)
    poissonsn = Djeun(nnn1, nnn2, nnn3, nnn4, nnn5, nnn6, nnn7)
    pouletsb = Djeun(bbbb1, bbbb2, bbbb3, bbbb4, bbbb5, bbbb6, bbbb7)
    
    poissonsb = Djeun(bbb1, bbb2, bbb3, bbb4, bbb5, bbb6, bbb7)
    boeufsb = Djeun(bb1, bb2, bb3, bb4, bb5, bb6, bb7)
    poissonshhh = Djeun(hhh1hh, hhh2hh, hhh3hh, hhh4hh, hhh5hh, hhh6hh, hhh7hh)
    pouletsbb = Djeun(bbbb1b, bbbb2b, bbbb3b, bbbb4b, bbbb5b, bbbb6b, bbbb7b)
    crevb = Djeun(bbbbbbb1, bbbbbbb2, bbbbbbb3, bbbbbbb4, bbbbbbb5, bbbbbbb6, bbbbbbb7)
    crevc = Djeun(ccccccc1, ccccccc2, ccccccc3, ccccccc4, ccccccc5, ccccccc6, ccccccc7)
    poissonsu = Djeun(uuu1, uuu2, uuu3, uuu4, uuu5, uuu6, uuu7)
    pouletsbbb = Djeun(bbbb1bb, bbbb2bb, bbbb3bb, bbbb4bb, bbbb5bb, bbbb6bb, bbbb7bb)
    pouletsbbbb = Djeun(bbbb1bbb, bbbb2bbb, bbbb3bbb, bbbb4bbb, bbbb5bbb, bbbb6bbb, bbbb7bbb)
    pigeone = Djeun(eeeeee1, eeeeee2, eeeeee3, eeeeee4, eeeeee5, eeeeee6, eeeeee7)
    pigeonf = Djeun(ffffff1, ffffff2, ffffff3, ffffff4, ffffff5, ffffff6, ffffff7)
    moutonsh = Djeun(h1, h2, h3, h4, h5, h6, h7)
    moutonsb = Djeun(b1, b2, b3, b4, b5, b6, b7)
    # poissonsg = Djeun(ggg1, ggg2, ggg3, ggg4, ggg5, ggg6, ggg7)
    dindef = Djeun(fffff1, fffff2, fffff3, fffff4, fffff5, fffff6, fffff7)
    moutonsbb = Djeun(b1b, b2b, b3b, b4b, b5b, b6b, b7b)
    crevd = Djeun(ddddddd1, ddddddd2, ddddddd3, ddddddd4, ddddddd5, ddddddd6, ddddddd7)

    # dindeg = Djeun(ggggg1, ggggg2, ggggg3, ggggg4, ggggg5, ggggg6, ggggg7)
    
    pouletsd = Djeun(dddd1, dddd2, dddd3, dddd4, dddd5, dddd6, dddd7)
    poissonse = Djeun(eee1, eee2, eee3, eee4, eee5, eee6, eee7)
    creve = Djeun(eeeeeee1, eeeeeee2, eeeeeee3, eeeeeee4, eeeeeee5, eeeeeee6, eeeeeee7)
    crevf = Djeun(fffffff1, fffffff2, fffffff3, fffffff4, fffffff5, fffffff6, fffffff7)


    moutonsc = Djeun(c1, c2, c3, c4, c5, c6, c7)
    poissonsc = Djeun(ccc1, ccc2, ccc3, ccc4, ccc5, ccc6, ccc7)
    moutonsf = Djeun(f1, f2, f3, f4, f5, f6, f7)
    
    dindea = Djeun(aaaaa1, aaaaa2, aaaaa3, aaaaa4, aaaaa5, aaaaa6, aaaaa7)
    
    dinded = Djeun(ddddd1, ddddd2, ddddd3, ddddd4, ddddd5, ddddd6, ddddd7)
    dindee = Djeun(eeeee1, eeeee2, eeeee3, eeeee4, eeeee5, eeeee6, eeeee7)
    
    
    pigeona = Djeun(aaaaaa1, aaaaaa2, aaaaaa3, aaaaaa4, aaaaaa5, aaaaaa6, aaaaaa7)
    pigeonb = Djeun(bbbbbb1, bbbbbb2, bbbbbb3, bbbbbb4, bbbbbb5, bbbbbb6, bbbbbb7)
    
    crevg = Djeun(ggggggg1, ggggggg2, ggggggg3, ggggggg4, ggggggg5, ggggggg6, ggggggg7)


    pouletsc = Djeun(cccc1, cccc2, cccc3, cccc4, cccc5, cccc6, cccc7)
    poissonsd = Djeun(ddd1, ddd2, ddd3, ddd4, ddd5, ddd6, ddd7)
    boeufsd = Djeun(dd1, dd2, dd3, dd4, dd5, dd6, dd7)
    boeufsc = Djeun(cc1, cc2, cc3, cc4, cc5, cc6, cc7)
    moutonsd = Djeun(d1, d2, d3, d4, d5, d6, d7)
    pigeonc = Djeun(cccccc1, cccccc2, cccccc3, cccccc4, cccccc5, cccccc6, cccccc7)
    pigeond = Djeun(dddddd1, dddddd2, dddddd3, dddddd4, dddddd5, dddddd6, dddddd7)
    # poissonsl = Djeun(lll1, lll2, lll3, lll4, lll5, lll6, lll7)

    poissonsf = Djeun(fff1f, fff2f, fff3f, fff4f, fff5f, fff6f, fff7f)
    boeufse = Djeun(ee1, ee2, ee3, ee4, ee5, ee6, ee7)
    dindeb = Djeun(bbbbb1, bbbbb2, bbbbb3, bbbbb4, bbbbb5, bbbbb6, bbbbb7)
    dindec = Djeun(ccccc1, ccccc2, ccccc3, ccccc4, ccccc5, ccccc6, ccccc7)
    poissonsm = Djeun(mmm1, mmm2, mmm3, mmm4, mmm5, mmm6, mmm7)
    moutonse = Djeun(e1, e2, e3, e4, e5, e6, e7)
    

    boeufsf = Djeun(ff1, ff2, ff3, ff4, ff5, ff6, ff7)
    poissonsff = Djeun(fff1f, fff2f, fff3f, fff4f, fff5f, fff6f, fff7f)
    

    boeufsg = Djeun(gg1, gg2, gg3, gg4, gg5, gg6, gg7)
    

    boeufsh = Djeun(hh1, hh2, hh3, hh4, hh5, hh6, hh7)
    
    boeufsm = Djeun(mm1, mm2, mm3, mm4, mm5, mm6, mm7)
    boeufsi = Djeun(ii1, ii2, ii3, ii4, ii5, ii6, ii7)
    poissonsi = Djeun(iii1, iii2, iii3, iii4, iii5, iii6, iii7)
    boeufsa = Djeun(aa1, aa2, aa3, aa4, aa5, aa6, aa7)

    boeufsj = Djeun(jj1, jj2, jj3, jj4, jj5, jj6, jj7)
    # poissonsj = Djeun(jjj1, jjj2, jjj3, jjj4, jjj5, jjj6, jjj7)
    

    boeufsk = Djeun(kk1, kk2, kk3, kk4, kk5, kk6, kk7)
    # poissonsk = Djeun(kkk1, kkk2, kkk3, kkk4, kkk5, kkk6, kkk7)

    boeufsl = Djeun(ll1, ll2, ll3, ll4, ll5, ll6, ll7)
    
    boeufsn = Djeun(nn1, nn2, nn3, nn4, nn5, nn6, nn7)  

    Sessionalchemy.add(poissondd)
    Sessionalchemy.add(mouton)
    Sessionalchemy.add(poulet)
    Sessionalchemy.add(poissonse)
    Sessionalchemy.add(poissonshhhh)  
    Sessionalchemy.add(dindea)
    Sessionalchemy.add(poissonst)
    Sessionalchemy.add(creva)
    Sessionalchemy.add(boeuf)
    Sessionalchemy.add(poissonsp)
    Sessionalchemy.add(pigeona)
    Sessionalchemy.add(poissonshhh)
    Sessionalchemy.add(poissonsh)
    Sessionalchemy.add(creve)
    Sessionalchemy.add(poissonddd)
    Sessionalchemy.add(dindeb)
    Sessionalchemy.add(poissonspp)
    Sessionalchemy.add(poissonsq)
    Sessionalchemy.add(boeufsm) 
    Sessionalchemy.add(dindef)
    Sessionalchemy.add(poissonshh)
    Sessionalchemy.add(poissondddd)
    Sessionalchemy.add(crevb)
    Sessionalchemy.add(poissonsqs)
    Sessionalchemy.add(pigeonb)
    Sessionalchemy.add(poissonsr)
    Sessionalchemy.add(poissonsppp)
    Sessionalchemy.add(moutonsa)
    Sessionalchemy.add(boeufsn) 
    # Sessionalchemy.add(poissonsg)
    Sessionalchemy.add(poissonss)
    Sessionalchemy.add(pouletsa)
    # Sessionalchemy.add(dindeg)
    Sessionalchemy.add(poissonsa)
    Sessionalchemy.add(crevf)

    Sessionalchemy.add(dindec)
    Sessionalchemy.add(pigeonc)
    
    Sessionalchemy.add(moutonsb)
    Sessionalchemy.add(crevc)
    Sessionalchemy.add(poissonso)
    # Sessionalchemy.add(moutonsi)
    Sessionalchemy.add(poissonsn)
    Sessionalchemy.add(dinded)
    Sessionalchemy.add(poissonsf)
    Sessionalchemy.add(pouletsb)
    Sessionalchemy.add(pigeonf)
    Sessionalchemy.add(moutonsbb)
    Sessionalchemy.add(crevd)
    Sessionalchemy.add(pouletsbbbb)
    Sessionalchemy.add(boeufsc)
    Sessionalchemy.add(poissonsb)
    Sessionalchemy.add(pigeond)
    Sessionalchemy.add(pouletsbbb)
    Sessionalchemy.add(boeufsb)
    Sessionalchemy.add(dindee)
    # Sessionalchemy.add(moutonsj)
    Sessionalchemy.add(poisson)
    Sessionalchemy.add(moutonsc)  
    Sessionalchemy.add(pigeone)
    Sessionalchemy.add(pouletsbbb)
    Sessionalchemy.add(poissonsff)
    Sessionalchemy.add(poissonsu)
    Sessionalchemy.add(pouletsd)
    Sessionalchemy.add(poissonsc)
    Sessionalchemy.add(moutonsh)
      
    # Sessionalchemy.add(moutonsk)
    Sessionalchemy.add(poissonsb)
    Sessionalchemy.add(poissonsm)
    Sessionalchemy.add(crevg)
    Sessionalchemy.add(poissonsd)
    Sessionalchemy.add(boeufsd)
    Sessionalchemy.add(moutonsd) 
    Sessionalchemy.add(moutonsg) 
    # Sessionalchemy.add(poissonsl)
    # Sessionalchemy.add(poissonsj)
    Sessionalchemy.add(moutonse)
    # Sessionalchemy.add(moutonsl)
    Sessionalchemy.add(boeufsk)
    # Sessionalchemy.add(poissonsk)
    Sessionalchemy.add(boeufse)
    Sessionalchemy.add(boeufsl) 
    Sessionalchemy.add(boeufsi)
    Sessionalchemy.add(poissonsi)
    Sessionalchemy.add(boeufsh)
    Sessionalchemy.add(moutonsgg)
    
    # Sessionalchemy.add(moutonsf)
    Sessionalchemy.add(boeufsj)
    Sessionalchemy.add(boeufsg)
    
    Sessionalchemy.add(boeufsa)
    Sessionalchemy.add(pouletsc)
    Sessionalchemy.add(boeufsf)
    Sessionalchemy.add(poissonsf)
    Sessionalchemy.commit()


######################################  Fonction deploiment
@app.before_request  
def before_request():
 if request.url.startswith('http://'):
  url = request.url.replace('http://', 'https://', 1)
  code = 301            
  return redirect(url, code=code)  
       
    

from collections import defaultdict
def data_by_team(data):
    d = defaultdict(lambda: [0,0])
    for team, number in data:  
        d[team][0] += 1
        d[team][1] += number
    return dict(d)

from collections import defaultdict
def data_by_team1(data):
    d = defaultdict(lambda: [0,0,0,0])
    for team, number, j, k in data:
        # print(team)
        # print(d[team][2])
        d[team][0] += 1
        d[team][1] += number
        d[team][2] += j
        d[team][3] += k
    return dict(d)
################################### LOGIN #################################

#### code pour generer un secret key: python -c 'import os; print(os.urandom(16))'

  
idd = []           

@app.route('/login/', methods=['GET', 'POST'])
def login():
    count=0   
    # Output message if something goes wrong...
    msg = ''
    # Check if "username" and "password" POST requests exist (user submitted form)
    if request.method == 'POST' and 'telephon' in request.form and 'password' in request.form :
      # Create variables for easy access
        telephon = request.form['telephon']
        pass_wordd = request.form['password']

        hash = []
        exits = Sessionalchemy.query(User.id)\
            .filter(User.telephone==telephon)\
            .scalar()
        if exits != None:
            data = Sessionalchemy.query(User)\
                .filter(User.telephone == telephon)\
                .all() 
              
            for i in data:
                pass_word = i.pass_word
                hash.append(pass_word)
            # pass_words = session.get(pass_word, None)
            val1 = check_password_hash(hash[0], pass_wordd)
            if val1 == True:
                person = Sessionalchemy.query(User)\
                    .filter(User.telephone == telephon)\
                    .filter(User.pass_word == hash[0])\
                    .all()
                role = ""
                for i in person:
                    id = i.id
                    prenom = i.prenom
                    nom = i.nom
                    telephone = i.telephone
                    adresse = i.adresse
                    regions = i.region
                    mail = i.mail
                    etat_passwordd = i.etat_password
                    role = i.role
                    stat = i.statut
                    session['loggedin'] = True
                    session['id'] = id
                    session['role'] = role
                    session['statut'] = stat
                if role == "admin" or role == "investisseur":
                    if etat_passwordd == "false":
                        return redirect(url_for('changepassword'))
                    else:
                        return redirect(url_for('chef'))
                elif role == "comptable":
                    return redirect(url_for('comptable'))
                elif role == "markting" :
                    return redirect(url_for('markting'))
                else:
                    if etat_passwordd == "false":
                        return redirect(url_for('changepassword'))
                    else:
                        return redirect(url_for('caissier'))                    
            else:
                flash("Le mot de pass tapé est incorrect!", "danger")
        else:
            flash("Le numéro téléphone est incorrect!", "danger")                        
    return render_template('login_register.html')

####################################### PARTIE CHEF ###############################

@app.route('/chef', methods=['GET', 'POST'])
def chef():
    moi = []
    liste = []
    new_commande = []
    if session.get('role') == "admin" or session.get('role') == "investisseur" and session.get('statut') == "actif":
        new_commande = []
        comm = Sessionalchemy.query(Commandes)\
            .distinct()\
            .count()
        commnonapp = Sessionalchemy.query(Commandes)\
            .filter(Commandes.appeller=='NON')\
            .distinct()\
            .count()
        commnonliv = Sessionalchemy.query(Commandes)\
            .filter(Commandes.livrer=='NON')\
            .distinct()\
            .count()

        Commandesss = Sessionalchemy.query(Commandes)\
            .filter(Commandes.livrer=='NON')\
            .order_by(Commandes.date_commande)\
            .all()
        
        commliv = Sessionalchemy.query(Commandes)\
            .filter(Commandes.livrer=='OUI')\
            .distinct()\
            .count()

        for i in Commandesss:
            id = i.id
            prenomcl = i.prenom_client
            nomcl = i.nom_client
            numcl = i.num_client
            datecomm = i.date_commande
            fraislivraison = i.frais_livraison
            adresslivraison = i.adresslivraison
            datealiver = i.date_a_livrer
            appeler = i.appeller
            livrer = i.livrer
            tots = i.total
            # valtot = int(fraislivraison) + int(prix_total)
            # valtot = ""
            new_commande.append((id, prenomcl, nomcl, numcl, datecomm, fraislivraison, adresslivraison, datealiver, appeler,livrer, tots))
        
        id_user = session.get('id',None)
        me = Sessionalchemy.query(User)\
            .filter(User.id==id_user)\
            .all()
        for i in me:
            prenme = i.prenom
            nomme = i.nom
            role = i.role
            pourc = i.pourcentage
            mont = i.montant
            ca = i.conseil_admin
            moi.append((prenme,nomme, role, pourc, mont, ca))
    else:
        flash("désolé, vous ne pouvez pas accéder à ce compte", "danger")
        return redirect(url_for('login')) 
    
    return render_template('visualchef.html', new_commande=new_commande, comm=comm, commnonapp=commnonapp, commnonliv=commnonliv, commliv=commliv, moi=moi)
@app.route('/investisseurss', methods=['GET', 'POST'])
def investisseurss():
    investis = Sessionalchemy.query(User)\
            .filter(User.statut == 'actif')\
            .filter(User.role == 'investisseur')\
            .all()
    investisseur = []
    for i in investis:
        prenme = i.prenom
        nomme = i.nom
        telephone = i.telephone
        mail = i.mail
        pourc = i.pourcentage
        adress = i.adresse
        cni = i.cni
        mont = i.montant
        ca = i.conseil_admin
        investisseur.append((prenme,nomme, telephone,mail, adress, cni, pourc, mont, ca))
    moi = []
    id_user = session.get('id',None)
    me = Sessionalchemy.query(User)\
        .filter(User.id==id_user)\
        .all()
    for i in me:
        prenme = i.prenom
        nomme = i.nom
        role = i.role
        pourc = i.pourcentage
        mont = i.montant
        ca = i.conseil_admin
        moi.append((prenme,nomme, role, pourc, mont, ca))
    return render_template('investisseurss.html', investisseur=investisseur, moi=moi)

@app.route('/commchef', methods=['GET', 'POST'])
def commchef():
    moi = []
    liste = []
    new_commande = []
    if session.get('role') == "admin" or session.get('role') == "investisseur" and session.get('statut') == "actif":

        Commandesss = Sessionalchemy.query(Commandes)\
            .filter(Commandes.livrer=='NON')\
            .order_by(Commandes.date_commande)\
            .all()
        
        commliv = Sessionalchemy.query(Commandes)\
            .filter(Commandes.livrer=='OUI')\
            .distinct()\
            .count()

        for i in Commandesss:
            id = i.id
            prenomcl = i.prenom_client
            nomcl = i.nom_client
            numcl = i.num_client
            datecomm = i.date_commande
            fraislivraison = i.frais_livraison
            adresslivraison = i.adresslivraison
            datealiver = i.date_a_livrer
            appeler = i.appeller
            livrer = i.livrer
            tots = i.total
            # valtot = int(fraislivraison) + int(prix_total)
            # valtot = ""
            new_commande.append((id, prenomcl, nomcl, numcl, datecomm, fraislivraison, adresslivraison, datealiver, appeler,livrer, tots))
        
        # if len(new_commande) != 0:
        #     new_commandes = new_commande
        # else:
        #     return redirect(url_for('admin'))

        id_user = session.get('id',None)
        me = Sessionalchemy.query(User)\
            .filter(User.id==id_user)\
            .all()
        for i in me:
            prenme = i.prenom
            nomme = i.nom
            role = i.role
            moi.append((prenme,nomme,role))
        
    else:
        flash("désolé, vous ne pouvez pas accéder à ce compte", "danger")
        return redirect(url_for('login')) 
    
    return render_template('commchef.html', new_commande=new_commande, moi=moi, commliv=commliv)

@app.route('/commmarkting', methods=['GET', 'POST'])
def commmarkting():
    moi = []
    liste = []
    new_commande = []
    if session.get('role') == "markting" and session.get('statut') == "actif":

        Commandesss = Sessionalchemy.query(Commandes)\
            .filter(Commandes.livrer=='NON')\
            .order_by(Commandes.date_commande)\
            .all()
        
        commliv = Sessionalchemy.query(Commandes)\
            .filter(Commandes.livrer=='OUI')\
            .distinct()\
            .count()

        for i in Commandesss:
            id = i.id
            prenomcl = i.prenom_client
            nomcl = i.nom_client
            numcl = i.num_client
            datecomm = i.date_commande
            fraislivraison = i.frais_livraison
            adresslivraison = i.adresslivraison
            datealiver = i.date_a_livrer
            appeler = i.appeller
            livrer = i.livrer
            tots = i.total
            new_commande.append((id, prenomcl, nomcl, numcl, datecomm, fraislivraison, adresslivraison, datealiver, appeler,livrer, tots))
        id_user = session.get('id',None)
        me = Sessionalchemy.query(User)\
            .filter(User.id==id_user)\
            .all()
        for i in me:
            prenme = i.prenom
            nomme = i.nom
            moi.append((prenme,nomme))
        
    else:
        flash("désolé, vous ne pouvez pas accéder à ce compte", "danger")
        return redirect(url_for('login')) 
    
    return render_template('commmarkting.html', new_commande=new_commande, moi=moi, commliv=commliv)

@app.route('/caissierchef', methods=['GET', 'POST'])
def caissierchef():
    if session.get('role')== "admin" or session.get('role')== "investisseur":
        id_user = session.get('id',None)
        moi = []
        me = Sessionalchemy.query(User)\
            .filter(User.id==id_user)\
            .all()
        for i in me:
            prenme = i.prenom
            nomme = i.nom
            moi.append((prenme,nomme))


        caissierr = Sessionalchemy.query(User)\
            .filter(or_(User.role=="caissier", User.role=="comptable", User.role=="markting"))\
            .filter(User.etat_password=="true")\
            .filter(User.statut=="actif")\
            .all()
        lecaissier = []
        for i in caissierr:
            idc = i.id
            prenme = i.prenom
            nomme = i.nom
            phone = i.telephone
            role = i.role
            sal = i.salaire
            lecaissier.append((idc, prenme,nomme, phone, role, sal))


    else:
        flash("désolé, vous ne pouvez pas accéder à ce compte", "danger")
        return redirect(url_for('login')) 
    
    return render_template('caissierchef.html', lecaissier=lecaissier, moi=moi)


@app.route('/detailcaissier/<string:idap>', methods=['GET'])
def detailcaissier(idap):
    if session.get('role') == "admin" or session.get('role') == "investisseur" and session.get('statut') == "actif": 

        caissierr = Sessionalchemy.query(User)\
            .filter(User.id==idap)\
            .all()
        lecaissier = []
        for i in caissierr:
            idc = i.id
            prenme = i.prenom
            nomme = i.nom
            phone = i.telephone
            email = i.mail
            adresse = i.adresse
            cni = i.cni
            datnaiss = i.datenaissance
            lecaissier.append((idc, prenme,nomme, phone, email, adresse, cni, datnaiss))

        id_user = session.get('id',None)
        moi = []
        myclient = []
        me = Sessionalchemy.query(User)\
            .filter(User.id==id_user)\
            .all()
        for i in me:
            prenme = i.prenom
            nomme = i.nom
            moi.append((prenme,nomme))
        
        commandday = Sessionalchemy.query(Commandes)\
            .filter(Commandes.user_id == idap)\
            .filter(Commandes.valider == "OUI")\
            .all()
        
        to = 0
        vis = []
        for i in commandday:
            id = i.id
            datecomm = i.date_commande
            fraislivraison = i.frais_livraison
            adresslivraison = i.adresslivraison
            tots = int(i.total) + int(fraislivraison)
            prov = i.provenance
            to = to + tots
            tots = int(i.total) + int(fraislivraison)
            vis.append((datecomm, tots))
            
        val = data_by_team(vis)
        myvalue= []
        for j in val:
            myvalue.append((j,val[j][0],val[j][1]))
    else:
        flash("désolé, vous ne pouvez pas accéder à ce compte", "danger")
        return redirect(url_for('login'))
    return render_template('detailcaissier.html', lecaissier=lecaissier, moi=moi, myvalue=myvalue)

@app.route('/commandechef', methods=['GET', 'POST'])
def commandechef():
    if session.get('role') == "admin" or session.get('role') == "investisseur" and session.get('statut') == "actif":
        page = int(request.args.get('page', 1))
        per_page = 10
        offset = (page - 1) * per_page
        id_user = session.get('id',None)
        moi = []
        me = Sessionalchemy.query(User)\
            .filter(User.id==id_user)\
            .all()
        for i in me:
            prenme = i.prenom
            nomme = i.nom
            moi.append((prenme,nomme))


        commandday = Sessionalchemy.query(Commandes)\
            .filter(Commandes.valider == "OUI")\
            .all()
        
        to = 0
        vis = []
        total = []
        for i in commandday:
            id = i.id
            datecomm = i.date_commande
            fraislivraison = i.frais_livraison
            adresslivraison = i.adresslivraison
            tots = int(i.total) + int(fraislivraison)
            prov = i.provenance
            depense = i.depense
            benef = i.benefice
            to = to + tots
            total.append(to)
            vis.append((datecomm, tots, depense, benef))
        
        df = pd.DataFrame([(d.date_commande, d.prenom_client, d.nom_client, d.num_client, d.total, d.frais_livraison, d.adresslivraison, d.date_a_livrer, d.provenance, d.depense, d.benefice) for d in commandday], 
                  columns=['date_commande', 'prenom_client', 'nom_client', 'num_client', 'total_commande_produit', 'fraislivraison', 'adresslivraison', 'date_a_livrer', 'provenance', 'depense', 'benefice'])
        df[["total_commande_produit", "fraislivraison"]] = df[["total_commande_produit", "fraislivraison"]].apply(pd.to_numeric)  # convertir colonne en numeric
        sum_column = df["total_commande_produit"] + df["fraislivraison"]
        df["revenus_total_commande"] = sum_column   # créer une nouvelle colonne pour le revenu total
        
        df['date_commande'] = pd.to_datetime(df['date_commande'])  # convertir en date la colonne
        revenus_mois = df.groupby(df['date_commande'].dt.strftime('%B'))['revenus_total_commande'].sum().sort_values()
        dep_mois = df.groupby(df['date_commande'].dt.strftime('%B'))['depense'].sum().sort_values()
        benef_mois = df.groupby(df['date_commande'].dt.strftime('%B'))['benefice'].sum().sort_values()
        nbr_revenus_mois = df.groupby(df['date_commande'].dt.strftime('%B'))['revenus_total_commande'].count()

        revenus = revenus_mois.index.tolist()

        # lieu_proprietaire = df.groupby(df['date_commande'].dt.strftime('%B'), 'num_client')['revenus_total_commande'].sum().nlargest(3)
        # print(lieu_proprietaire)
        montant_mois = [d for d in revenus_mois]
        depense_mois = [d for d in dep_mois]
        benefice_mois = [d for d in benef_mois]
        nbr = [d for d in nbr_revenus_mois]
        mois = [revenus, montant_mois, nbr, depense_mois, benefice_mois]  #
        # largest_states = lieu_proprietaire.index.tolist()
            
        val = data_by_team1(vis)
        myvalue= []
        nbrProd = 0
        montantProd = 0
        depenseProd = 0
        benefProd = 0
        pourcentageBenefProduit = 0

        for j in val:
            nbrProd = nbrProd + val[j][0]
            montantProd =montantProd + val[j][1]
            depenseProd = depenseProd + val[j][2]
            benefProd = benefProd + val[j][3] 
            pourcentageBenefProduit = pourcentageBenefProduit + (benefProd*100)/montantProd
            
            myvalue.append((j,val[j][0],val[j][1], val[j][2], val[j][3]))

        dep = Sessionalchemy.query(Depenses)\
            .order_by(Depenses.date)\
            .all()
        depens = []
        DepAutre = 0
        tottt = []
        for i in dep:
            montant = i.montant
            nbr = i.nombre
            tot = i.totaldepense
            DepAutre = DepAutre + int(tot)
            tottt.append(DepAutre)
            datee = i.date
            justif = i.justification
            depens.append((montant, nbr, tot, datee, justif))
        
        depTotal  = depenseProd + DepAutre
        benefTotal = montantProd - depTotal
        totalprod = []
        if montantProd !=0:
            pourcentageBenefTotal = (benefTotal*100)/montantProd
            totalprod = [nbrProd, montantProd, depenseProd, benefProd, DepAutre, str(round(pourcentageBenefProduit, 2)), depTotal, benefTotal, str(round(pourcentageBenefTotal, 2))]
        
        
        
    else:
        flash("désolé, vous ne pouvez pas accéder à ce compte", "danger")
        return redirect(url_for('login')) 

    total = len(myvalue)
    paginationUsers = myvalue[offset: offset + per_page]
    pagination = Pagination(page=page, per_page=per_page, total=total,
                            css_framework='bootstrap4')
    
    return render_template('commandechef.html',totalprod=totalprod, moi=moi, mois=mois, users=paginationUsers, page=page, per_page=per_page,pagination=pagination)


@app.route('/visualtotalchef', methods=['GET', 'POST'])
def visualtotalchef():
    if session.get('role') == "admin" or session.get('role') == "investisseur" and session.get('statut') == "actif":
        id_user = session.get('id',None)
        moi = []
        me = Sessionalchemy.query(User)\
            .filter(User.id==id_user)\
            .all()
        for i in me:
            prenme = i.prenom
            nomme = i.nom
            moi.append((prenme,nomme))


        commandday = Sessionalchemy.query(Commandes)\
            .filter(Commandes.valider == "OUI")\
            .all()
        
        to = 0
        vis = []
        total = []
        for i in commandday:
            id = i.id
            datecomm = i.date_commande
            fraislivraison = i.frais_livraison
            adresslivraison = i.adresslivraison
            tots = int(i.total) + int(fraislivraison)
            prov = i.provenance
            depense = i.depense
            benef = i.benefice
            to = to + tots
            total.append(to)
            vis.append((datecomm, tots, depense, benef))
        
        df = pd.DataFrame([(d.date_commande, d.prenom_client, d.nom_client, d.num_client, d.total, d.frais_livraison, d.adresslivraison, d.date_a_livrer, d.provenance, d.depense, d.benefice) for d in commandday], 
                  columns=['date_commande', 'prenom_client', 'nom_client', 'num_client', 'total_commande_produit', 'fraislivraison', 'adresslivraison', 'date_a_livrer', 'provenance', 'depense', 'benefice'])
        df[["total_commande_produit", "fraislivraison"]] = df[["total_commande_produit", "fraislivraison"]].apply(pd.to_numeric)  # convertir colonne en numeric
        sum_column = df["total_commande_produit"] + df["fraislivraison"]
        df["revenus_total_commande"] = sum_column   # créer une nouvelle colonne pour le revenu total
        
        df['date_commande'] = pd.to_datetime(df['date_commande'])  # convertir en date la colonne
        revenus_mois = df.groupby(df['date_commande'].dt.strftime('%B'))['revenus_total_commande'].sum().sort_values()
        dep_mois = df.groupby(df['date_commande'].dt.strftime('%B'))['depense'].sum().sort_values()
        benef_mois = df.groupby(df['date_commande'].dt.strftime('%B'))['benefice'].sum().sort_values()
        nbr_revenus_mois = df.groupby(df['date_commande'].dt.strftime('%B'))['revenus_total_commande'].count()

        revenus = revenus_mois.index.tolist()

        # lieu_proprietaire = df.groupby(df['date_commande'].dt.strftime('%B'), 'num_client')['revenus_total_commande'].sum().nlargest(3)
        # print(lieu_proprietaire)
        montant_mois = [d for d in revenus_mois]
        depense_mois = [d for d in dep_mois]
        benefice_mois = [d for d in benef_mois]
        nbr = [d for d in nbr_revenus_mois]
        mois = [revenus, montant_mois, nbr, depense_mois, benefice_mois]  #
        # largest_states = lieu_proprietaire.index.tolist()
            
        val = data_by_team1(vis)
        myvalue= []
        nbrProd = 0
        montantProd = 0
        depenseProd = 0
        benefProd = 0
        pourcentageBenefProduit = 0

        for j in val:
            nbrProd = nbrProd + val[j][0]
            montantProd =montantProd + val[j][1]
            depenseProd = depenseProd + val[j][2]
            benefProd = benefProd + val[j][3] 
            pourcentageBenefProduit = pourcentageBenefProduit + (benefProd*100)/montantProd
            
            myvalue.append((j,val[j][0],val[j][1], val[j][2], val[j][3]))

        
        
    else:
        flash("désolé, vous ne pouvez pas accéder à ce compte", "danger")
        return redirect(url_for('login')) 
    
    return render_template('visualtotalchef.html', myvalue=myvalue, moi=moi, mois=mois)



@app.route('/produitchef', methods=['GET', 'POST'])
def produitchef():
    if session.get('role') == "admin" or session.get('role') == "investisseur" and session.get('statut') == "actif":
        # prod = Sessionalchemy.query(Produits)\
        #     .order_by(desc(Produits.prix_total)).limit(10)\
        #     .all()
        prod = Sessionalchemy.query(Produits)\
            .all()
        
        viandemouton = 0
        viandeboeuf = 0
        poissondo = 0
        poulo = 0
        for i in prod:
            categ = i.categories
            if categ == 'viandemouton':
                viandemouton = viandemouton + int(i.prix_total)
            elif categ == 'viandeboeuf':
                viandeboeuf = viandeboeuf + int(i.prix_total)
            elif categ == 'poulets':
                poulo = poulo + int(i.prix_total)
            elif categ == 'poissons':
                poissondo = poissondo + int(i.prix_total)

        produit = [('viande mouton', viandemouton),('poissons', poissondo), ('poulets', poulo), ('viande boeuf', viandeboeuf)]
        
        id_user = session.get('id',None)
        moi = []
        me = Sessionalchemy.query(User)\
            .filter(User.id==id_user)\
            .all()
        for i in me:
            prenme = i.prenom
            nomme = i.nom
            moi.append((prenme,nomme))
    else:
        flash("désolé, vous ne pouvez pas accéder à ce compte", "danger")
        return redirect(url_for('login')) 
    
    return render_template('produitchef.html', produit=produit, moi=moi)

@app.route('/depensechef', methods=['GET', 'POST'])
def depensechef():
    if session.get('role') == "admin" or session.get('role') == "investisseur" and session.get('statut') == "actif":
        dep = Sessionalchemy.query(Depenses)\
            .order_by(Depenses.date)\
            .all()
        depens = []
        for i in dep:
            montant = i.montant
            nbr = i.nombre
            tot = i.totaldepense
            datee = i.date
            justif = i.justification
            depens.append((montant, nbr, tot, datee, justif))
            
        id_user = session.get('id',None)
        moi = []
        me = Sessionalchemy.query(User)\
            .filter(User.id==id_user)\
            .all()
        for i in me:
            prenme = i.prenom
            nomme = i.nom
            moi.append((prenme,nomme))
    else:
        flash("désolé, vous ne pouvez pas accéder à ce compte", "danger")
        return redirect(url_for('login')) 
    
    return render_template('depensechef.html', depens=depens, moi=moi)



    # return render_template('user.html', new_commandes=new_commandes, comm=comm, commnonapp=commnonapp, commnonliv=commnonliv, commliv=commliv, moi=moi)

# page administrateurs avec 0 modéle invalide
@app.route('/admin', methods=['GET', 'POST'])
def admin():   
    if session.get('role') == "admin" or session.get('role') == "investisseur" and session.get('statut') == "actif":
        moi=[]
        comm = Sessionalchemy.query(Commandes)\
            .distinct()\
            .count()
        commnonapp = Sessionalchemy.query(Commandes)\
            .filter(Commandes.appeller=='NON')\
            .distinct()\
            .count()
        commnonliv = Sessionalchemy.query(Commandes)\
            .filter(Commandes.livrer=='NON')\
            .distinct()\
            .count()
        commliv = Sessionalchemy.query(Commandes)\
            .filter(Commandes.livrer=='OUI')\
            .distinct()\
            .count()
        id_user = session.get('id',None)
        me = Sessionalchemy.query(User)\
            .filter(User.id==id_user)\
            .all()
        for i in me:
            prenme = i.prenom
            nomme = i.nom
            moi.append((prenme,nomme))
        
    else:
        flash("désolé, vous ne pouvez pas accéder à ce compte", "danger")
        return redirect(url_for('login')) 
    return render_template('admin.html', comm=comm, commnonapp=commnonapp, commnonliv=commnonliv, moi=moi, commliv=commliv)

@app.route('/liste', methods=['GET', 'POST'])
def liste():   
    if session.get('role') == "admin" or session.get('role') == "investisseur" and session.get('statut') == "actif":
        moi = []
        prod = []
        prods = []
        djeun = Sessionalchemy.query(Djeun)\
            .all()

        for i in djeun:
            idd = i.id
            photo = i.photo
            nom = i.nom
            prix = i.prix

            prod.append((idd, photo,nom,prix))
        if len(prod) != 0:
            prods = prod
        else:
            return redirect(url_for('listevide'))
        
        id_user = session.get('id',None)
        me = Sessionalchemy.query(User)\
            .filter(User.id==id_user)\
            .all()
        for i in me:
            prenme = i.prenom
            nomme = i.nom
            moi.append((prenme,nomme))    
    else:
        flash("désolé, vous ne pouvez pas accéder à ce compte", "danger")
        return redirect(url_for('login')) 
    return render_template('liste.html', prods=prods, moi=moi)

@app.route('/listevide', methods=['GET', 'POST'])
def listevide(): 
    if session.get('role') == "admin" or session.get('role') == "investisseur" and session.get('statut') == "actif":
        moi = []  
        comm = Sessionalchemy.query(Commande)\
            .distinct()\
            .count()
        id_user = session.get('id',None)
        me = Sessionalchemy.query(User)\
            .filter(User.id==id_user)\
            .all()
        for i in me:
            prenme = i.prenom
            nomme = i.nom
            moi.append((prenme,nomme))
    else:
        flash("désolé, vous ne pouvez pas accéder à ce compte", "danger")
        return redirect(url_for('login')) 
    return render_template('listevide.html', moi=moi)

@app.route('/ajoutprod', methods=['GET', 'POST'])
def ajoutprod():   
    if session.get('role') == "admin" or session.get('role') == "investisseur" and session.get('statut') == "actif":
        nom = request.form.get("nameq")
        prix = request.form.get("prixq")
        old_price = request.form.get("oldprice")       
        images = request.files.getlist("imageq")
        types = request.form.get("nom") 
        description = request.form.get("description")
        valeur = request.form.get("vallss")
        if len(images)>1:
            flash("Vous avez selectionnez plus d'une image, revoyez svp la saisie.","danger")  
            
        else:
            for image in images:
                if image.filename == "":
                    flash("Entrez svp une image", "avertissement")
                    return redirect(request.url)

                if allowed_image(image.filename):
                    filename = secure_filename(image.filename)
                    filenam, file_extension = os.path.splitext(filename)
                    hash = generate_password_hash(filenam)
                    hash=hash[-10:]
                    d = hash+file_extension
                    image.save(os.path.join(app.config["IMAGE_UPLOADS"], d))
                    djeunn = Djeun(nom, d, prix, old_price, types, description, valeur)
                    Sessionalchemy.add(djeunn)
                    Sessionalchemy.commit()
                
                    flash("Un nouveau produit est ajouté sur la liste.","succes")   
                else:
                    flash("Un nouveau produit est ajouté sur la liste.","succes") 
    else:
        flash("désolé, vous ne pouvez pas accéder à ce compte", "danger")
        return redirect(url_for('login')) 
    return render_template('ajoutprod.html')


@app.route('/detaillant/<string:idap>', methods=['GET'])
def detaillant(idap):
    if session.get('id'):  
        dets = Sessionalchemy.query(Produits)\
            .filter(Produits.commandes_id==idap)
        details = []
        moi = []
        myclient = []
        tt = 0
        for i in dets:
            id = i.id
            nom = i.nom
            photo = i.photo
            prix_unitaire = i.prix_unitaire
            quantite = i.quantite
            fraislivraison = i.fraislivraison
            prix_total = i.prix_total
            tt = tt + prix_total
            tts = int(fraislivraison) + tt
            
            details.append((id, nom, photo, prix_unitaire, quantite, prix_total, fraislivraison, tt, tts))
        id_user = session.get('id',None)
        me = Sessionalchemy.query(User)\
            .filter(User.id==id_user)\
            .all()
        for i in me:
            prenme = i.prenom
            nomme = i.nom
            moi.append((prenme,nomme))
        
        lui = Sessionalchemy.query(Commandes)\
            .filter(Commandes.id==idap)\
            .all()
        for i in lui:
            prenomcli = i.prenom_client
            nomcli = i.nom_client
            zonecli = i.adresslivraison
            datecommande = i.date_commande
            datelivrai = i.date_a_livrer
            numcli = i.num_client
            myclient.append((prenomcli,nomcli, numcli, zonecli, datecommande, datelivrai))
    else:
        flash("désolé, vous ne pouvez pas accéder à ce compte", "danger")
        return redirect(url_for('login'))
    return render_template('detaillant.html', details=details, moi=moi, myclient=myclient)


@app.route('/detaillantchef/<string:idap>', methods=['GET'])
def detaillantchef(idap):
    if session.get('id'):  
        dets = Sessionalchemy.query(Produits)\
            .filter(Produits.commandes_id==idap)
        details = []
        moi = []
        myclient = []
        tt = 0
        for i in dets:
            id = i.id
            nom = i.nom
            photo = i.photo
            prix_unitaire = i.prix_unitaire
            quantite = i.quantite
            fraislivraison = i.fraislivraison
            prix_total = i.prix_total
            tt = tt + prix_total
            tts = int(fraislivraison) + tt
            
            details.append((id, nom, photo, prix_unitaire, quantite, prix_total, fraislivraison, tt, tts))
        id_user = session.get('id',None)
        me = Sessionalchemy.query(User)\
            .filter(User.id==id_user)\
            .all()
        for i in me:
            prenme = i.prenom
            nomme = i.nom
            moi.append((prenme,nomme))
        
        lui = Sessionalchemy.query(Commandes)\
            .filter(Commandes.id==idap)\
            .all()
        for i in lui:
            prenomcli = i.prenom_client
            nomcli = i.nom_client
            zonecli = i.adresslivraison
            datecommande = i.date_commande
            datelivrai = i.date_a_livrer
            numcli = i.num_client
            myclient.append((prenomcli,nomcli, numcli, zonecli, datecommande, datelivrai))
    else:
        flash("désolé, vous ne pouvez pas accéder à ce compte", "danger")
        return redirect(url_for('login'))
    return render_template('detaillantchef.html', details=details, moi=moi, myclient=myclient)



@app.route('/appel/<string:idap>', methods=['GET'])
def appel(idap):
    if session.get('role') == "caissier" and session.get('statut') == "actif":  
        val = Sessionalchemy.query(Commandes)\
                .filter(Commandes.id==idap).first()
        val.appeller = 'OUI'
        Sessionalchemy.commit()
        flash("Vous avez déja appelé le client, veuillez bien noter son adresse.","succes")
        return redirect(url_for('commandeenligne'))
    else:
        flash("désolé, vous ne pouvez pas accéder à ce compte", "danger")
        return redirect(url_for('login'))

@app.route('/livr/<string:id_ap>', methods=['GET'])
def livr(id_ap):
    if session.get('role') == "caissier" and session.get('statut') == "actif":
        session['idcomds'] = id_ap
        prodq = Sessionalchemy.query(Produits)\
            .filter(Produits.commandes_id==id_ap)\
            .all()
        idboutique = Sessionalchemy.query(User)\
            .filter(User.id==session.get('id'))\
            .first()
        boutiqstockage = Sessionalchemy.query(Boutiques)\
            .filter(Boutiques.id==idboutique.boutique_id)\
            .first()
        vald = Sessionalchemy.query(Commandes)\
            .filter(Commandes.id==id_ap)\
            .first()
        # boutiquelivrer = boutiqstockage.nomboutique
        # users = idboutique.prenom + " " + idboutique.nom
        # now = datetime.datetime.now()
        # date_commande = now.strftime("%Y-%m-%d %H:%M:%S")
        # details = []
        # for i in prodq:
        #     id = i.id
        #     categories = i.categories
        #     nom = i.nom
        #     print(nom)
        #     prixunitaire = i.prix_unitaire
        #     quantite = i.quantite
        #     fraislivraison = i.fraislivraison
            
        #     details.append((id, categories, nom, prixunitaire, quantite, fraislivraison))
        #     obj = Sessionalchemy.query(Stockages)\
        #         .filter(Stockages.boutique == boutiquelivrer)\
        #         .filter(Stockages.categorieprod == categories)\
        #         .filter(Stockages.nomprod == nom)\
        #         .filter(Stockages.statut == "encours")\
        #         .order_by(Stockages.id.desc()).first()
        #     typesstockage = "vente"
        #     # print(boutiquelivrer, categories, nom)
        #     punitvente = prixunitaire
        #     utilisateur = users
        #     if obj != None: 
        #         qtyss = int(obj.quantitestocker) - int(quantite)
        #         if qtyss > 0:
        #             punitachat = obj.puachat
        #             benef = (int(punitvente) - int(punitachat))*int(quantite)
        #             statutstockage = "encours"
        #             stock = Stockages(boutiquelivrer,qtyss, categories, nom, punitachat, punitvente, benef, id_ap,utilisateur, typesstockage, statutstockage, date_commande)
        #             Sessionalchemy.add(stock)
        #             obj.statut = "encoursold"
        #             vald.livrer = 'OUI'
        #             Sessionalchemy.commit()
        #         elif qtyss == 0:
        #             punitachat = obj.puachat
        #             benef = (int(punitvente) - int(punitachat))*int(quantite)
        #             statutstockage = "finish"
        #             stock = Stockages(boutiquelivrer,qtyss, categories, nom, punitachat, punitvente, benef, id_ap,utilisateur, typesstockage, statutstockage, date_commande)
        #             Sessionalchemy.add(stock)
        #             Sessionalchemy.commit()
        #             obj.statut = "encoursold"
        #             vald.livrer = 'OUI'
        #             Sessionalchemy.commit()
        #             flash("Attention, le stockage de {}".format(nom)+ " est fini. \r\n Veuillez contacter le responsable de distribution pour l'approvisionnement", "danger")
        #         else:
        #             nextstock = Sessionalchemy.query(Stockages)\
        #                 .filter(Stockages.boutique == boutiquelivrer)\
        #                 .filter(Stockages.categorieprod == categories)\
        #                 .filter(Stockages.nomprod == nom)\
        #                 .filter(Stockages.statut == "enattente")\
        #                 .first()
        #             if nextstock == None:
        #                 flash("Attention, il ne reste que {}".format(obj.quantitestocker)+ " pour le produit {}".format(nom)+" dans votre stockage.", "danger")
        #             else:
        #                 punitachat = nextstock.puachat
        #                 benef = (int(punitvente) - int(punitachat))*int(obj.quantitestocker)
        #                 statutstockage = "finish"
        #                 stock = Stockages(boutiquelivrer,0, categories, nom, punitachat, punitvente, benef, id_ap,utilisateur, typesstockage, statutstockage, date_commande)
        #                 Sessionalchemy.add(stock)
        #                 Sessionalchemy.commit()
        #                 nextstock.statut = "encours"
        #                 obj.statut = "encoursold"
        #                 Sessionalchemy.commit()
        #                 qtyss2 = int(nextstock.quantitestocker) - abs(qtyss)
        #                 if qtyss2 > 0:
        #                     punitachat = nextstock.puachat
        #                     benef = (int(punitvente) - int(punitachat))*abs(qtyss)
        #                     statutstockage = "encours"
        #                     stock = Stockages(boutiquelivrer,qtyss2, categories, nom, punitachat, punitvente, benef, id_ap,utilisateur, typesstockage, statutstockage, date_commande)
        #                     Sessionalchemy.add(stock)
        #                     Sessionalchemy.commit()
        #                     nextstock.statut = "encoursold"
        #                     vald.livrer = 'OUI'
        #                     Sessionalchemy.commit()
        #                 elif qtyss2 == 0:
        #                     punitachat = nextstock.puachat
        #                     benef = (int(punitvente) - int(punitachat))*abs(qtyss)
        #                     statutstockage = "finish"
        #                     stock = Stockages(boutiquelivrer,qtyss2, categories, nom, punitachat, punitvente, benef, id_ap,utilisateur, typesstockage, statutstockage, date_commande)
        #                     Sessionalchemy.add(stock)
        #                     Sessionalchemy.commit()
        #                     nextstock.statut = "encoursold"
        #                     vald.livrer = 'OUI'
        #                     Sessionalchemy.commit()
        #                     flash("Attention, le stockage de {}".format(nom)+ " est fini. \r\n Veuillez contacter le responsable de distribution pour l'approvisionnement", "danger")
        #                 else:
        #                     nextstock2 = Sessionalchemy.query(Stockages)\
        #                         .filter(Stockages.boutique == boutiquelivrer)\
        #                         .filter(Stockages.categorieprod == categories)\
        #                         .filter(Stockages.nomprod == nom)\
        #                         .filter(Stockages.statut == "enattente")\
        #                         .first()
                        
        #                     if nextstock2 == None:
        #                         flash("Attention, il ne reste que {}".format(nextstock.quantitestocker)+ " {}".format(nom)+" dans votre stockage.", "danger")
        #                         return redirect(url_for('commandeenligne'))
        #                     else:
        #                         punitachat = nextstock2.puachat
        #                         benef = (int(punitvente) - int(punitachat))*int(nextstock.quantitestocker)
        #                         statutstockage = "finish"
        #                         stock = Stockages(boutiquelivrer,qtyss, categories, nom, punitachat, punitvente, benef, id_ap,utilisateur, typesstockage, statutstockage, date_commande)
        #                         Sessionalchemy.add(stock)
        #                         Sessionalchemy.commit()
        #                         nextstock2.statut = "encours"
        #                         Sessionalchemy.commit()
        #                         qtyss3 = int(nextstock.quantitestocker) - abs(qtyss)
        #     else:
        #         flash("désolé, le produit {}".format(nom)+ " n'existe pas dans votre boutique,\r\n Cependant la commande ne peut pas etre livrée. \r\n veuillez contacter le responsable de distribution", "danger")
        return redirect(url_for('depensecommande'))

    else:
        flash("désolé, vous ne pouvez pas accéder à ce compte", "danger")
        return redirect(url_for('login'))
@app.route('/depensecommande', methods=['GET', 'POST'])
def depensecommande(): 
    idcom = session.get('idcomds')
    Commandq = Sessionalchemy.query(Commandes)\
            .filter(Commandes.id==idcom)\
            .all()
    details = []
    for i in Commandq:
        id = i.id
        prenomclient = i.prenom_client
        nomclient = i.nom_client
        tel = i.num_client
        adress = i.adresslivraison
        total = i.total
        frais = i.frais_livraison
        
        details.append((id, prenomclient, nomclient, tel, adress, total, frais))
    return render_template('depensecommande.html', details=details)

@app.route('/enregistrdepensecommande', methods=['GET', 'POST'])
def enregistrdepensecommande():  
    if session.get('role') == "caissier" and session.get('statut') == "actif":
        depense = request.form['dep']
        idcom = session.get('idcomds')
        idbout = Sessionalchemy.query(User.boutique_id)\
                    .filter(User.id==session.get('id'))\
                    .one() 
        commaqx = Sessionalchemy.query(Boutiques).filter_by(id=idbout[0]).first()
            
        Command = Sessionalchemy.query(Commandes)\
            .filter(Commandes.id==idcom)\
            .all()
        val = Sessionalchemy.query(Commandes)\
            .filter(Commandes.id==idcom)\
            .first()

        comid = []
        for i in Command:
            total = i.total
            frais = i.frais_livraison
            dep = i.depense
            comid.append((total,frais, dep))

        benef = int(comid[0][0]) + int(comid[0][1]) - int(depense)
        val.depense = int(depense)
        val.benefice = benef
        val.user_id = session.get('id')
        val.livrer = 'OUI'
        val.boutique_id = idbout[0]
        Sessionalchemy.commit()
        flash("Vous avez déja livré le client, veuillez bien noter ses informations.","succes")
        return redirect(url_for('commandeenligne'))
    else:
        flash("désolé, vous ne pouvez pas accéder à ce compte", "danger")
        return redirect(url_for('login'))
    

@app.route('/supprim/<string:id_ap>', methods=['GET'])
def supprim(id_ap):
    if session.get('role') == "caissier" and session.get('statut') == "actif":

        Sessionalchemy.query(Produits)\
            .filter(Produits.commandes_id==id_ap).delete()
        # Sessionalchemy.delete(supp_prod)
        Sessionalchemy.commit()
        
        supprime = Sessionalchemy.query(Commandes).filter_by(id=id_ap).one()
        Sessionalchemy.delete(supprime)
        Sessionalchemy.commit()
        flash("La commande a été supprimée.","succes")   

        return redirect(url_for('commandeenligne'))
    else:
        flash("désolé, vous ne pouvez pas accéder à ce compte", "danger")
        return redirect(url_for('login'))


@app.route('/valider/<string:id_ap>', methods=['GET'])
def valider(id_ap):
    if session.get('id'): 
        session['modifid'] = id_ap
        modif = []
        admin = Sessionalchemy.query(Djeun)\
            .filter(Djeun.id==id_ap)
        for i in admin:
            photo = i.photo
            nom = i.nom
            prix = i.prix
            val = i.valeur
            modif.append((photo,nom,prix, val))
        
        # admin.user_id = ids
        Sessionalchemy.commit()
    
    else:
        flash("désolé, vous ne pouvez pas accéder à ce compte", "danger")
        return redirect(url_for('login'))

    return render_template('modif.html', modif=modif)

@app.route('/modif', methods=['GET', 'POST'])
def modif():
    if session.get('role') == "admin" or session.get('role') == "investisseur" and session.get('statut') == "actif":
        if request.method == 'POST':
            ids = session.get('modifid')
            nom = request.form.get("nameq")
            prix = request.form.get("prixq")
            val = Sessionalchemy.query(Djeun)\
                .filter(Djeun.id==ids).first()
            val.nom = nom
            val.prix = prix
            Sessionalchemy.commit()
            flash("Le produit est modifié avec succé.","succes")
        return redirect(url_for('chef'))
    else:
        flash("désolé, vous ne pouvez pas accéder à ce compte", "danger")
        return redirect(url_for('login'))

@app.route('/supprimer/<string:id_ap>', methods=['GET'])
def supprimer(id_ap):
    if session.get('role') == "admin" or session.get('role') == "investisseur" and session.get('statut') == "actif":
        phot=[]
        val =Sessionalchemy.query(Djeun)\
            .filter(Djeun.id==id_ap)\
                .all()
        for i in val:
            photo = i.photo
            phot.append(photo)
        Sessionalchemy.query(Djeun)\
            .filter(Djeun.id==id_ap).delete()
        Sessionalchemy.commit()
        
        os.remove(os.path.join(app.config["IMAGE_UPLOADS"], phot[0]))

        flash("Le produit est supprimé de la liste.","danger")

        return redirect(url_for('chef'))

    else:
        flash("désolé, vous ne pouvez pas accéder à ce compte", "danger")
        return redirect(url_for('login'))

# Page Accueil
@app.route('/products-to-json', methods=['GET'])
def accueilappli():
    appli = []
    applii = []
    djeuns = Sessionalchemy.query(Djeun)\
        .all()
    for i in djeuns:   
        photo = i.photo
        nom = i.nom
        prix = str(i.prix)
        id = i.id
        old = i.old_prix
        val = i.valeur
        typecommande = i.type_commande
        applii.append({'id':id, 'nom': nom, 'image':'https://www.sendawal.com/static/images/'+photo, 'prix': prix, 'typecommande': typecommande})
    return jsonify({'products':applii})
    
@app.route('/')
def accueil():
    #li.clear()
    ret = "yes accueil"
    page = int(request.args.get('page', 1))
    per_page = 15
    offset = (page - 1) * per_page

    djeuns = Sessionalchemy.query(Djeun)\
        .all()
    djeun = []
    appli = []
    for i in djeuns:   
        photo = i.photo
        nom = i.nom
        prix = i.prix
        id = i.id
        old = i.old_prix
        val = i.valeur
        typecommande = i.type_commande
        djeun.append((photo, nom, prix, id, old, val))
        appli.append({"id":id, "nom": nom, "image":"https://www.sendawal.com/static/images/"+photo, "prix": prix, "typecommande": typecommande})
    total = len(djeun)
    paginationUsers = djeun[offset: offset + per_page]
    pagination = Pagination(page=page, per_page=per_page, total=total,
                            css_framework='bootstrap4')

   
    return render_template('index.html', users=paginationUsers, page=page, per_page=per_page,pagination=pagination)


# #####*********************************************barre de recherche****************************************************#########
# @app.route('/search/<val>')
@app.route('/search', methods=['GET', 'POST'])
def search():
    page = int(request.args.get('page', 1))
    per_page = 15
    offset = (page - 1) * per_page
    searchh=[]
    if request.method == "POST":
        li.clear()
        vall=request.form.get('vals')   
        val=vall.lower()
        search = "%{}%".format(val) 
        valeurs = Sessionalchemy.query(Djeun)\
            .filter(or_(Djeun.nom.ilike(search), cast( Djeun.prix, String ).ilike(search)))\
            .all()

        datas = []
        for i in valeurs:
            id = i.id
            nom = i.nom
            photo = i.photo
            prix = i.prix
            val = i.valeur
            datas.append((photo, nom, prix, id, val))

        if len(datas)!=0: 
            total = len(datas)
            paginationUsers=datas[offset: offset + per_page]
            pagination = Pagination(page=page, per_page=per_page, total=total,
                                    css_framework='bootstrap4')
        else:
            return  redirect(url_for('vide')) 
    else:
        return  redirect(url_for('accueil')) 
    return render_template('catalog_fullwidth.html', users=paginationUsers, page=page, per_page=per_page,pagination=pagination)
@app.route('/vide',methods=['GET', 'POST'])
def vide():
    return render_template('vide.html')

####################### Partie détail ##############################
@app.route('/detail&<string:id_ap>', methods=['GET'])
def detail(id_ap):
    page = int(request.args.get('page', 1))
    per_page = 15
    offset = (page - 1) * per_page

    dieun=[]
    poissons = []
    modes = Sessionalchemy.query(Djeun)\
        .filter(Djeun.id==id_ap)\
        .all()
    tell = []
    for i in modes: 
        id = i.id  
        photo = i.photo
        nom = i.nom
        prix = i.prix
        description = i.description
        types = i.type_commande
        val = i.valeur
        old = i.old_prix
        tell.append((id, photo, nom, prix, description, types, val, old))
    if len(tell) != 0:
        dieun = tell
    else:
        return  redirect(url_for('accueil'))
    
    values = Sessionalchemy.query(Djeun)\
        .all()
    val = []
    for i in values: 
        id = i.id  
        photo = i.photo
        nom = i.nom
        old = i.old_prix
        prix = i.prix
        valq = i.valeur
        val.append((id, photo, nom, prix, old, valq))
    
    if len(val) != 0:
        values = val
    else:
        return  redirect(url_for('accueil'))
    

    total = len(values)
    paginationUsers = values[offset: offset + per_page]
    pagination = Pagination(page=page, per_page=per_page, total=total,
                            css_framework='bootstrap4')

    return render_template('detailss.html',dieun=dieun, users=paginationUsers, page=page, per_page=per_page,pagination=pagination)

@app.route('/panier')
def panier():

    return render_template('panier.html')


@app.route('/checkout')
def checkout():
    li.clear()

    return render_template('checkout.html')

@app.route('/about')
def about():
    li.clear()
    print("ok")

    return render_template('about.html')

@app.route('/catalog')
def catalog():
     page = int(request.args.get('page', 1))
     per_page = 15
     offset = (page - 1) * per_page
     #li.clear()

     vendeur=[]
     id_user = session.get('id',None)

     djeuns = Sessionalchemy.query(Djeun)\
          .all()
     djeun = []
     for i in djeuns:   
          photo = i.photo
          nom = i.nom
          prix = i.prix
          id = i.id
          val = i.valeur
          old = i.old_prix
          djeun.append((photo, nom, prix, id, val, old))
                                        
     total = len(djeun)
     paginationUsers = djeun[offset: offset + per_page]
     pagination = Pagination(page=page, per_page=per_page, total=total,
                            css_framework='bootstrap4')

     return render_template('catalog_fullwidth.html', users=paginationUsers, page=page, per_page=per_page,pagination=pagination)

@app.route('/acheter&<string:idap>', methods = ['GET','POST'])
def acheter(idap):
     achat=[]
     session['idachet'] = idap 
     val =Sessionalchemy.query(Djeun)\
          .filter(Djeun.id==idap)\
          .all()
     for i in val:
        photo = i.photo
        nom = i.nom
        prix = i.prix
        id = i.id
        val = i.valeur
        achat.append((photo,nom,prix,id, val))
     if len(achat)!=0:
        achat = achat
     else:
        return  redirect(url_for('catalog'))

     return render_template('shopping_cart.html', achat=achat)

@app.route('/contact', methods=['GET', 'POST'])
def contact():
    if request.method == 'POST':
        nom = request.form['nom']
        prenom = request.form['prenom']
        mail = request.form['mail']
        Comment = request.form['Comment']
        msg = Message(nom, prenom, mail, Comment)
        Sessionalchemy.add(msg)
        Sessionalchemy.commit()
        flash("Merci, votre message a été bien envoyé!", "succes")

    return render_template('contact.html')

@app.route('/command', methods=['GET', 'POST'])
def command():
     comm=[]
     if request.method == 'POST':
        idacheter = session.get('idachet')
        prixs = request.form['prix']
        qte = request.form['qte']
        prixtot = int(prixs)*int(qte)
        appeller = "NON"
        livrer = "NON"

        val =Sessionalchemy.query(Djeun)\
            .filter(Djeun.id==idacheter)\
            .all()
        for i in val:
            photo = i.photo
            nom = i.nom
            prix = i.prix
            comm.append((photo,nom,prix))
        a = comm[0]

     return redirect(url_for('accueil'))

@app.route('/getpanier', methods=['GET','POST'])
def getpanier():
    if request.method == "POST": 
        datas = []
        carte = request.form.get("qqq")
        # print(carte)
        # Nettoyage des données
        carte1 = carte.replace("[", "")
        carte2 = carte1.replace("]", "")
        carte3 = carte2.split("},")
        s = carte3[-1].replace("}", "")
        carte3[-1] = s
        
        for j in carte3:
            val1 = j+"}"
            val2 = json.loads(val1)
            datas.append(val2)
        session['data'] = datas
        return redirect(url_for('finalpanier'))

@app.route('/finalpanier')
def finalpanier():
    datas = session.get('data')
    if len(datas) == 0:
        return redirect(url_for('accueil'))
    else:
        tot = 0
        qqte = 0
        mypanier = []

        for i in datas:
            produit = i['val2']
            prix = i['val3']
            qty = i['val4']
            total = i['val5']
            tot = tot + int(total)
            qqte = qqte + int(qty)
            photoo = Sessionalchemy.query(Djeun.photo)\
                    .filter(Djeun.nom==produit)\
                    .first()
            mypanier.append((photoo,produit,prix,qty, total))
        # nbr = len(mypanier)

    return render_template('finalpanier.html', mypanier=mypanier, tot=tot, qqte=qqte)



@app.route('/option&<string:id_option>', methods=['GET'])
def option(id_option):
    datas = session.get('datas')
    tot = 0
    if datas == None:
        return redirect(url_for('catalog'))
    else:
        for i in datas:
            total = i['val5']
            tot = tot + int(total) + id_option

    liste_res = []
    liste_res.append({"categorie":tot})
    if len(liste_res) != 0 :
        return jsonify(liste_res)
    else:
        redirect(url_for('catalog'))



@app.route('/frompanier' , methods=['GET','POST'])
def frompanier():
    if request.method == "POST":
        datas = session.get('data')
        fraislivraison = ""
        # datas = session.get('datas')
        if len(datas) == 0:
            return redirect(url_for('catalog'))
        else:
            # datas = li[0]
            idss = session.get('id')
            ids = Sessionalchemy.query(User).filter_by(id=idss).first()
            datelivraison = request.form.get("category")
            fraislivraison1 = request.form.get("valchoose1")
            fraislivraison2 = request.form.get("valchoose2")
            addlivraison = request.form.get("add")
            prenom = request.form.get("prenom")
            nom = request.form.get("nom") 
            telephoneq = request.form.get("telephone")
            alphabets = string.ascii_letters + string.digits
            vals = generate_password_hash(alphabets)
            vals2 = vals[-10:]
            provenance = "en_ligne"
            valider = "Non"

            if fraislivraison1 == "0":
                fraislivraison = fraislivraison2
            else:
                fraislivraison = fraislivraison1


            appeller = "NON"
            livrer = "NON"
            now = datetime.datetime.now()
            date_commande = now.strftime("%Y-%m-%d %H:%M:%S")
            boutique = ""
            idbout = Sessionalchemy.query(Boutiques).filter_by(nomboutique=boutique).first()

            
            # print(fraislivraison)
            if datas == None:
                return redirect(url_for('catalog'))
            else:
                if fraislivraison != "0":
                    tots = 0
                    for i in datas:
                        total = i['val5']
                        tots = tots + int(total)
                    dep = 0
                    benef = 0
                    comm = Commandes(prenom, nom, telephoneq, date_commande,tots, fraislivraison, addlivraison,datelivraison,appeller,livrer, provenance, valider,dep, benef, vals2, ids, idbout)
                    Sessionalchemy.add(comm)
                    Sessionalchemy.commit()


                    tas = 0
                    for i in datas:
                        produit = i['val2']
                        prix = i['val3']
                        qty = i['val4']
                        total = i['val5']
                        tas = tas + int(total)
                        photoo = Sessionalchemy.query(Djeun.photo)\
                                .filter(Djeun.nom==produit)\
                                .first()  
                        categorie = Sessionalchemy.query(Djeun.type_commande)\
                                .filter(Djeun.nom==produit)\
                                .first() 
                    
                        idcom = Sessionalchemy.query(Commandes.id)\
                                .filter(Commandes.num_client==telephoneq)\
                                .filter(Commandes.date_commande==date_commande)\
                                .filter(Commandes.valunique==vals2)\
                                .first() 
                        comma = Sessionalchemy.query(Commandes).filter_by(id=idcom[0]).first()
                        prod = Produits(produit, photoo, prix, qty, total,fraislivraison,tas, comma, categorie)
                        Sessionalchemy.add(prod)
                        Sessionalchemy.commit()
                        # session.pop('datas', None)
                    #li.clear()
                    
                    flash("Félicitation, vous avez fait votre commande avec succès, notre équipe vous contactera pour la livraison!", "succes")
                    return redirect(url_for('catalog'))
                
                else:
                    flash("Vous avez oublié de choisir votre zone de livraison", "danger")
                    return redirect(url_for('finalpanier'))
    # return redirect(url_for('catalog'))
        


####################### CATEGORISATIONS 

@app.route('/poissons', methods=['GET', 'POST'])
def poissons():
    li.clear()

    page = int(request.args.get('page', 1))
    per_page = 15
    offset = (page - 1) * per_page

    djeuns = Sessionalchemy.query(Djeun)\
        .filter(Djeun.type_commande=="poissons")\
        .all()
        
    djeun = []
    for i in djeuns:   
        photo = i.photo
        nom = i.nom
        prix = i.prix
        id = i.id
        val = i.valeur
        old = i.old_prix
        djeun.append((photo, nom, prix, id, val, old))
                                    
    total = len(djeun)
    paginationUsers = djeun[offset: offset + per_page]
    pagination = Pagination(page=page, per_page=per_page, total=total,
                        css_framework='bootstrap4')

    return render_template('poissons.html', users=paginationUsers, page=page, per_page=per_page,pagination=pagination)


@app.route('/poulets', methods=['GET', 'POST'])
def poulets():
    page = int(request.args.get('page', 1))
    per_page = 15
    offset = (page - 1) * per_page

    djeuns = Sessionalchemy.query(Djeun)\
        .filter(Djeun.type_commande=="poulets")\
        .all()
        
    djeun = []
    for i in djeuns:   
        photo = i.photo
        nom = i.nom
        prix = i.prix
        id = i.id
        val = i.valeur
        old = i.old_prix
        djeun.append((photo, nom, prix, id, val, old))
                                    
    total = len(djeun)
    paginationUsers = djeun[offset: offset + per_page]
    pagination = Pagination(page=page, per_page=per_page, total=total,
                        css_framework='bootstrap4')

    return render_template('poulets.html', users=paginationUsers, page=page, per_page=per_page,pagination=pagination)

@app.route('/dindes', methods=['GET', 'POST'])
def dindes():
    page = int(request.args.get('page', 1))
    per_page = 15
    offset = (page - 1) * per_page

    djeuns = Sessionalchemy.query(Djeun)\
        .filter(Djeun.type_commande=="dindes")\
        .all()
        
    djeun = []
    for i in djeuns:   
        photo = i.photo
        nom = i.nom
        prix = i.prix
        id = i.id
        val = i.valeur
        old = i.old_prix
        djeun.append((photo, nom, prix, id, val, old))
                                    
    total = len(djeun)
    paginationUsers = djeun[offset: offset + per_page]
    pagination = Pagination(page=page, per_page=per_page, total=total,
                        css_framework='bootstrap4')

    return render_template('dindes.html', users=paginationUsers, page=page, per_page=per_page,pagination=pagination)

@app.route('/pigeons', methods=['GET', 'POST'])
def pigeons():
    page = int(request.args.get('page', 1))
    per_page = 15
    offset = (page - 1) * per_page

    djeuns = Sessionalchemy.query(Djeun)\
        .filter(Djeun.type_commande=="pigeons")\
        .all()
        
    djeun = []
    for i in djeuns:   
        photo = i.photo
        nom = i.nom
        prix = i.prix
        id = i.id
        val = i.valeur
        old = i.old_prix
        djeun.append((photo, nom, prix, id, val, old))
                                    
    total = len(djeun)
    paginationUsers = djeun[offset: offset + per_page]
    pagination = Pagination(page=page, per_page=per_page, total=total,
                        css_framework='bootstrap4')

    return render_template('pigeons.html', users=paginationUsers, page=page, per_page=per_page,pagination=pagination)



@app.route('/crevettes', methods=['GET', 'POST'])
def crevettes():
    page = int(request.args.get('page', 1))
    per_page = 15
    offset = (page - 1) * per_page

    djeuns = Sessionalchemy.query(Djeun)\
        .filter(Djeun.type_commande=="crevettes")\
        .all()
        
    djeun = []
    for i in djeuns:   
        photo = i.photo
        nom = i.nom
        prix = i.prix
        id = i.id
        val = i.valeur
        old = i.old_prix
        djeun.append((photo, nom, prix, id, val, old))
                                    
    total = len(djeun)
    paginationUsers = djeun[offset: offset + per_page]
    pagination = Pagination(page=page, per_page=per_page, total=total,
                        css_framework='bootstrap4')

    return render_template('crevettes.html', users=paginationUsers, page=page, per_page=per_page,pagination=pagination)

@app.route('/tablettes', methods=['GET', 'POST'])
def tablettes():
  

    return render_template('tablettes.html')
@app.route('/viandemouton', methods=['GET', 'POST'])
def viandemouton():
    li.clear()

    page = int(request.args.get('page', 1))
    per_page = 15
    offset = (page - 1) * per_page

    djeuns = Sessionalchemy.query(Djeun)\
        .filter(Djeun.type_commande=="viandemouton")\
        .all()
        
    djeun = []
    for i in djeuns:   
        photo = i.photo
        nom = i.nom
        prix = i.prix
        id = i.id
        val = i.valeur
        old = i.old_prix
        djeun.append((photo, nom, prix, id, val, old))
                                    
    total = len(djeun)
    paginationUsers = djeun[offset: offset + per_page]
    pagination = Pagination(page=page, per_page=per_page, total=total,
                        css_framework='bootstrap4')

    return render_template('viande.html', users=paginationUsers, page=page, per_page=per_page,pagination=pagination)

@app.route('/viandeboeuf', methods=['GET', 'POST'])
def viandeboeuf():
    li.clear()

    page = int(request.args.get('page', 1))
    per_page = 15
    offset = (page - 1) * per_page

    djeuns = Sessionalchemy.query(Djeun)\
        .filter(Djeun.type_commande=="viandeboeuf")\
        .all()
        
    djeun = []
    for i in djeuns:   
        photo = i.photo
        nom = i.nom
        prix = i.prix
        id = i.id
        val = i.valeur
        old = i.old_prix
        djeun.append((photo, nom, prix, id, val, old))
                                    
    total = len(djeun)
    paginationUsers = djeun[offset: offset + per_page]
    pagination = Pagination(page=page, per_page=per_page, total=total,
                        css_framework='bootstrap4')

    return render_template('viandeboeuf.html', users=paginationUsers, page=page, per_page=per_page,pagination=pagination)


@app.route('/motpassoublier', methods=['GET', 'POST'])
def motpassoublier():
    if request.method == 'POST':
        telephon = request.form['telephonforget']
        mail = request.form['emailforgot']
        exits = Sessionalchemy.query(User.id)\
            .filter(User.telephone==telephon)\
            .filter(User.mail==mail)\
            .scalar()
        if exits != None:
            session['phonoubli'] = telephon
            alphabet = string.ascii_letters + string.digits
            val = generate_password_hash(alphabet)
            val2 = val[-6:]
            

            ####  envoie code par mail

            import smtplib
            from smtplib import SMTPException

            fromaddr = 'senndawal2020@gmail.com'
            toaddrs  = mail
            
            msg = "\r\n".join([
                "From: senndawal2020@gmail.com",
                "To: {}".format(mail),
                "Subject: Recuperation de compte sen ndawal",
                "",
                "Code d'activation: {}".format(val2)
                ])
            username = 'senndawal2020@gmail.com'
            password = 's@nnd@w@lMNK'
            server = smtplib.SMTP('smtp.gmail.com:587')
            server.ehlo()
            server.starttls()
            server.login(username,password)
            server.sendmail(fromaddr, toaddrs, msg)
            server.quit()
            
            val = Sessionalchemy.query(User)\
                .filter(User.telephone == telephon)\
                .filter(User.mail == mail)\
                .first()
            val.code = val2
            Sessionalchemy.commit()
            flash("Un code vous a été envoyé par mail.","succes")
            return redirect(url_for('code'))
            
        else:
            flash("Aucun compte correspond au mail ou au numéro de telephone saisi", "danger")
            return redirect(url_for('login'))
    else:
        flash("désolé, vous ne pouvez pas accéder à ce compte", "danger")
        return redirect(url_for('login'))

@app.route('/code', methods=['GET', 'POST'])
def code():
    if request.method == 'POST':
        if session.get('phonoubli'):
            phon = session.get('phonoubli')
            code = request.form['codess']
            exits = Sessionalchemy.query(User.id)\
                .filter(User.telephone==phon)\
                .filter(User.code==code)\
                .scalar()
            if exits != None:
                val = Sessionalchemy.query(User)\
                    .filter(User.telephone == phon)\
                    .first()
                val.code = ""
                return redirect(url_for('passchange'))
            else:
                flash("Le code saisi est incorrect", "danger")
                return redirect(request.url)
        else:
            return redirect(url_for('login'))
    # else:
    #     return "ok" #redirect(url_for('login'))

    return render_template('code.html')

@app.route('/passchange', methods=['GET', 'POST'])
def passchange():
    if request.method == 'POST':
        if session.get('phonoubli'):
            pass_word = request.form.get('password')
            password22 = request.form.get('password22')
            if pass_word == password22:
                phon = session.get('phonoubli')
                pass_wordd = generate_password_hash(pass_word)
                exits = Sessionalchemy.query(User.id)\
                    .filter(User.telephone==phon)\
                    .scalar()
                if exits != None:
                    val = Sessionalchemy.query(User)\
                    .filter(User.telephone == phon)\
                    .first()
                    val.pass_word = pass_wordd
                    flash("Felicitation, votre mot de pass est maintenant mis à jour", "succes")
                    return redirect(url_for('login'))

            else:
                flash("Les mots de passes ne sont pas identiques!","danger") 
        else:
            return redirect(url_for('login'))

    return render_template('passchange.html')

@app.route('/message', methods=['GET', 'POST'])
def message():
    id_user = session.get('id',None)
    moi = []
    msgs = Sessionalchemy.query(Message)\
        .all()
        
    mess = []
    for i in msgs:   
        nom = i.nom
        prenom = i.prenom
        mail = i.mail
        message = i.message
        mess.append((nom, prenom, mail, message))
    me = Sessionalchemy.query(User)\
            .filter(User.id==id_user)\
            .all()
    for i in me:
        prenme = i.prenom
        nomme = i.nom
        moi.append((prenme,nomme))

    return render_template('message.html', mess=mess, moi=moi)


@app.route('/panierprods/<string:idap>', methods=['GET', 'POST'])
def panierprods(idap):
    dieun=[]
    modes = Sessionalchemy.query(Djeun)\
        .filter(Djeun.id==idap)\
        .all()
    tell = []
    for i in modes: 
        id = i.id  
        photo = i.photo
        nom = i.nom
        prix = i.prix
        description = i.description
        types = i.type_commande
        val = i.valeur
        tell.append((id, photo, nom, prix, description, types, val))
    if len(tell) != 0:
        dieun = tell
    else:
        return  redirect(url_for('catalog'))

    return render_template('panierprods.html', dieun=dieun)


@app.route('/comptablegeneral', methods=['GET', 'POST'])
def comptablegeneral():
    
    return render_template('comptablegeneral.html')



@app.route('/pageCaissier/<string:idap>', methods=['GET', 'POST'])
def pageCaissier(idap):
    if session.get('role')=="comptable" and session.get('statut')=="actif":
        lui = Sessionalchemy.query(User)\
            .filter(User.id==idap)\
            .all()
        lecaisier = []
        for i in lui:
            prenme = i.prenom
            nomme = i.nom
            idcaissier = i.id
            lecaisier.append((prenme, nomme, idcaissier))

        dt = datetime.datetime.today()
        todays_datetime = datetime.datetime(dt.year, dt.month, dt.day)
        commandday = Sessionalchemy.query(Commandes)\
            .filter(Commandes.user_id == idap)\
            .filter(Commandes.valider == "Non")\
            .all()
        
        moin24 = []
        mytotal = []
        to = 0
        ben = 0
        for i in commandday:
            id = i.id
            prenomcl = i.prenom_client
            nomcl = i.nom_client
            numcl = i.num_client
            datecomm = i.date_commande
            fraislivraison = i.frais_livraison
            adresslivraison = i.adresslivraison
            datealiver = i.date_a_livrer
            appeler = i.appeller
            livrer = i.livrer
            tots = int(i.total) + int(fraislivraison)
            prov = i.provenance
            dep = i.depense
            benef = i.benefice
            ben = ben + benef
            to = to + tots
            mytotal.append((to, ben))
            
            # valtot = int(fraislivraison) + int(prix_total)
            # valtot = ""
            
            moin24.append((id, prenomcl, nomcl, numcl, datecomm, fraislivraison, adresslivraison, datealiver, appeler,livrer, tots, prov, dep,benef))
        # print(mytotal[-1])
        
        # return render_template('listecommande.html', new_commande=new_commande, moin24=moin24, myvalue=myvalue, mytotal=mytotal)    

    else:
        flash("désolé, vous ne pouvez pas accéder à ce compte", "danger")
        return redirect(url_for('login'))

    return render_template('pageCaissier.html', moin24=moin24, mytotal=mytotal, lecaisier=lecaisier)


@app.route('/listecommande', methods=['GET', 'POST'])
def listecommande():
    id_user = session.get('id')
    mescommandes = Sessionalchemy.query(Commandes)\
        .filter(Commandes.user_id==id_user)\
        .filter(Commandes.valider == "Non")\
        .order_by(Commandes.date_commande)
        # .distinct()\
        # .count()
    new_commande = []
    vis = []
    for i in mescommandes:
        id = i.id
        prenomcl = i.prenom_client
        nomcl = i.nom_client
        numcl = i.num_client
        datecomm = i.date_commande
        fraislivraison = i.frais_livraison
        adresslivraison = i.adresslivraison
        datealiver = i.date_a_livrer
        appeler = i.appeller
        livrer = i.livrer
        # tots = i.total
        tots = int(i.total) + int(fraislivraison)
        prov = i.provenance
        depense = i.depense
        benefice = i.benefice
        vis.append((datecomm, tots))
        # valtot = int(fraislivraison) + int(prix_total)
        # valtot = ""
        new_commande.append((id, prenomcl, nomcl, numcl, datecomm, fraislivraison, adresslivraison, datealiver, appeler,livrer, tots, prov, depense, benefice))
    
    val = data_by_team(vis)
    myvalue= []
    for j in val:
        myvalue.append((j,val[j][0],val[j][1]))

    dt = datetime.datetime.today()
    todays_datetime = datetime.datetime(dt.year, dt.month, dt.day)
    commandday = Sessionalchemy.query(Commandes)\
        .filter(Commandes.valider == "Non")\
        .filter(Commandes.user_id==id_user)\
        .all()
    
    moin24 = []
    mytotal = []
    to = 0
    ben = 0
    for i in commandday:
        id = i.id
        prenomcl = i.prenom_client
        nomcl = i.nom_client
        numcl = i.num_client
        datecomm = i.date_commande
        fraislivraison = i.frais_livraison
        adresslivraison = i.adresslivraison
        datealiver = i.date_a_livrer
        appeler = i.appeller
        livrer = i.livrer
        tots = int(i.total) + int(fraislivraison)
        prov = i.provenance
        depense = i.depense
        benefice = i.benefice
        to = to + tots
        ben = ben + benefice
        mytotal.append((to, ben))
        
        moin24.append((id, prenomcl, nomcl, numcl, datecomm, fraislivraison, adresslivraison, datealiver, appeler,livrer, tots, prov, depense, benefice))
    # print(mytotal[-1])
    lui = Sessionalchemy.query(User)\
        .filter(User.id==id_user)\
        .all()
    lecaisier = []
    for i in lui:
        prenme = i.prenom
        nomme = i.nom
        idcaissier = i.id
        lecaisier.append((prenme, nomme, idcaissier))
    
    
    return render_template('listecommande.html', new_commande=new_commande, moin24=moin24, myvalue=myvalue, mytotal=mytotal, lecaisier=lecaisier)    

@app.route('/mydetail/<string:idap>', methods=['GET'])
def mydetail(idap):
    if session.get('id'):  
        dets = Sessionalchemy.query(Produits)\
            .filter(Produits.commandes_id==idap)
        details = []
        moi = []
        myclient = []
        tt = 0
        for i in dets:
            id = i.id
            nom = i.nom
            photo = i.photo
            prix_unitaire = i.prix_unitaire
            quantite = i.quantite
            fraislivraison = i.fraislivraison
            prix_total = i.prix_total
            tt = tt + prix_total
            tts = int(fraislivraison) + tt
            
            details.append((id, nom, photo, prix_unitaire, quantite, prix_total, fraislivraison, tt, tts))
        id_user = session.get('id',None)
        me = Sessionalchemy.query(User)\
            .filter(User.id==id_user)\
            .all()
        for i in me:
            prenme = i.prenom
            nomme = i.nom
            moi.append((prenme,nomme))
        
        lui = Sessionalchemy.query(Commandes)\
            .filter(Commandes.id==idap)\
            .all()
        
        for i in lui:
            prenomcli = i.prenom_client
            nomcli = i.nom_client
            zonecli = i.adresslivraison
            datecommande = i.date_commande
            datelivrai = i.date_a_livrer
            numcli = i.num_client
            myclient.append((prenomcli,nomcli, numcli, zonecli, datecommande, datelivrai))

        

    else:
        flash("désolé, vous ne pouvez pas accéder à ce compte", "danger")
        return redirect(url_for('login'))
    return render_template('mydetail.html', details=details, moi=moi, myclient=myclient)

@app.route('/mydetailtodaye/<string:idap>', methods=['GET'])
def mydetailtodaye(idap):
    if session.get('id'):  
        dets = Sessionalchemy.query(Produits)\
            .filter(Produits.commandes_id==idap)
        details = []
        moi = []
        myclient = []
        tt = 0
        for i in dets:
            id = i.id
            nom = i.nom
            photo = i.photo
            prix_unitaire = i.prix_unitaire
            quantite = i.quantite
            fraislivraison = i.fraislivraison
            prix_total = i.prix_total
            tt = tt + prix_total
            tts = int(fraislivraison) + tt
            
            details.append((id, nom, photo, prix_unitaire, quantite, prix_total, fraislivraison, tt, tts))
        id_user = session.get('id',None)
        me = Sessionalchemy.query(User)\
            .filter(User.id==id_user)\
            .all()
        for i in me:
            prenme = i.prenom
            nomme = i.nom
            moi.append((prenme,nomme))
        
        lui = Sessionalchemy.query(Commandes)\
            .filter(Commandes.id==idap)\
            .all()
        
        for i in lui:
            prenomcli = i.prenom_client
            nomcli = i.nom_client
            zonecli = i.adresslivraison
            datecommande = i.date_commande
            datelivrai = i.date_a_livrer
            numcli = i.num_client
            myclient.append((prenomcli,nomcli, numcli, zonecli, datecommande, datelivrai))

        

    else:
        flash("désolé, vous ne pouvez pas accéder à ce compte", "danger")
        return redirect(url_for('login'))
    return render_template('mydetailtodaye.html', details=details, moi=moi, myclient=myclient)


@app.route('/caissierdetailtodaye/<string:idap>', methods=['GET'])
def caissierdetailtodaye(idap):
    if session.get('id'):  
        dets = Sessionalchemy.query(Produits)\
            .filter(Produits.commandes_id==idap)
        details = []
        moi = []
        myclient = []
        tt = 0
        for i in dets:
            id = i.id
            nom = i.nom
            photo = i.photo
            prix_unitaire = i.prix_unitaire
            quantite = i.quantite
            fraislivraison = i.fraislivraison
            prix_total = i.prix_total
            tt = tt + prix_total
            tts = int(fraislivraison) + tt
            
            details.append((id, nom, photo, prix_unitaire, quantite, prix_total, fraislivraison, tt, tts))
        id_user = session.get('id',None)
        me = Sessionalchemy.query(User)\
            .filter(User.id==id_user)\
            .all()
        for i in me:
            prenme = i.prenom
            nomme = i.nom
            moi.append((prenme,nomme))
        
        lui = Sessionalchemy.query(Commandes)\
            .filter(Commandes.id==idap)\
            .all()
        
        for i in lui:
            prenomcli = i.prenom_client
            nomcli = i.nom_client
            zonecli = i.adresslivraison
            datecommande = i.date_commande
            datelivrai = i.date_a_livrer
            numcli = i.num_client
            myclient.append((prenomcli,nomcli, numcli, zonecli, datecommande, datelivrai))

        

    else:
        flash("désolé, vous ne pouvez pas accéder à ce compte", "danger")
        return redirect(url_for('login'))
    return render_template('caissierdetailtodaye.html', details=details, moi=moi, myclient=myclient)


@app.route('/comptable', methods=['GET', 'POST'])
def comptable():
    if session.get('role')=="comptable" and session.get('statut')=="actif":
        page = int(request.args.get('page', 1))
        per_page = 15
        offset = (page - 1) * per_page

        caisse = Sessionalchemy.query(User)\
        .filter(User.role=="caissier")\
        .filter(User.etat_password=="true")\
        .filter(User.statut=="actif")\
        .all()
        caissiers = []
        for i in caisse:
            id_caissier = i.id
            prenme = i.prenom
            nomme = i.nom
            phone = i.telephone
            caissiers.append((id_caissier, prenme, nomme, phone))

    else:
        flash("désolé, vous ne pouvez pas accéder à ce compte", "danger")
        return redirect(url_for('login'))
    
    total = len(caissiers)
    paginationUsers = caissiers[offset: offset + per_page]
    pagination = Pagination(page=page, per_page=per_page, total=total,
                            css_framework='bootstrap4')

    return render_template('comptable.html', users=paginationUsers, page=page, per_page=per_page,pagination=pagination)

@app.route('/creercaissier', methods=['GET', 'POST'])
def creercaissier():
    if session.get('role')=="comptable" and session.get('statut')=="actif":
        prenomc = request.form['prenomc']
        nomc = request.form['nomc']
        datenaissc = request.form['datenaissc']
        adressec = request.form['adressec']
        telephonec = request.form['telephonec']
        cnic = request.form['cnic']
        mailc = request.form['mailc']
        salaire = request.form['salairec']
        boutique = request.form['boutique']
        rolec = "caissier"
        codec = ""
        pourcentage = ""
        montant = ""
        conseil_admin = "non"
        # lettres min + lettres maj + chiffres
        pop = string.ascii_letters + string.digits
        # le mot de passe fera 6 caractères de long
        k = 6

        # sample retourne une portion aléatoire et de taille k à partir de la séquence pop
        passwd = ''.join(sample(pop, k))
        passc = generate_password_hash(passwd) #hachage password
        etat_pwd = False

        # bouts = Sessionalchemy.query(User.id)\
        #     .filter(User.nomboutique==boutique)\
        #     .one()

        idsbout = Sessionalchemy.query(Boutiques).filter_by(nomboutique=boutique).first()

        exitscni = Sessionalchemy.query(User.id)\
            .filter(User.cni==cnic)\
            .scalar()
        exitsnum = Sessionalchemy.query(User.id)\
            .filter(User.telephone==telephonec)\
            .scalar()
        if exitsnum != None:
            flash("Attention: Un compte est déja enregistré avec ce numéro", "danger")
            return redirect(url_for('comptable'))   
        else:
            if exitscni != None: 
                flash("Attention: Un compte est déja enregistrée avec cette CNI", "danger")
                return redirect(url_for('comptable'))
            else:
                # print(prenomc, nomc, datenaissc, adressec, cnic, mailc) 
                newcaissier = User(prenomc, nomc, telephonec, mailc, passc, adressec, region, codec, cnic, datenaissc, rolec, etat_pwd, statut, pourcentage, montant, salaire, conseil_admin, idsbout)
                Sessionalchemy.add(newcaissier)
                Sessionalchemy.commit()

                ####  envoie code par mail 

                import smtplib
                from smtplib import SMTPException

                fromaddr = 'senndawal2020@gmail.com'
                toaddrs  = mailc
                nomm = prenomc + " " + nomc
                
                msg = "\r\n".join([
                    "From: senndawal2020@gmail.com",
                    "To: {}".format(mailc),
                    "Subject: Activation de compte",
                    "",
                    "Bonjour {} ".format(nomm) + "votre mot de pass par defaut est: {}".format(passwd)
                    ])
                username = 'senndawal2020@gmail.com'
                password = 's@nnd@w@lMNK'
                server = smtplib.SMTP('smtp.gmail.com:587')
                server.ehlo()
                server.starttls()
                server.login(username,password)
                server.sendmail(fromaddr, toaddrs, msg)
                server.quit()
                
                flash("Félicitation: Vous avez enregistré un caissier avec succés", "succes")    
                return  redirect(url_for('comptable'))
            

    else:
        flash("désolé, vous ne pouvez pas accéder à ce compte", "danger")
        return redirect(url_for('login'))

    # return render_template('creercaissier.html')

@app.route('/caissier', methods=['GET', 'POST'])
def caissier():
    if session.get('id') != None and session.get('role') == "caissier":
       lst = []
       newcommm = session.get('newcomm')
       lst.append(newcommm)
    else:
        flash("désolé, vous ne pouvez pas accéder à ce compte", "danger")
        return redirect(url_for('login'))

    return render_template('caissier.html', lst=lst)


@app.route('/changepassword', methods=['GET', 'POST'])
def changepassword():
    if session.get('id'):
        print("ok")

    else:
        flash("désolé, vous ne pouvez pas accéder à ce compte", "danger")
        return redirect(url_for('login'))

    return render_template('changepassword.html')


@app.route('/bonpassword', methods=['GET', 'POST'])
def bonpassword():
    if session.get('id'):
        iddd = session.get('id')
        passworddd1 = request.form['passworddd1']
        passworddd2 = request.form['passworddd2']

        if passworddd1 == passworddd2:
            if len(passworddd1) >=6:
                passsword = generate_password_hash(passworddd1) #hachage password 
                # Update password and etat_password
                vall = Sessionalchemy.query(User)\
                .filter(User.id == iddd)\
                .first()

                rul = Sessionalchemy.query(User)\
                    .filter(User.id == iddd)\
                    .all()
                role =""
                for i in rul:
                    role = i.role
                
                # a_user = session.query(User).filter(User.id == iddd).one()
                vall.pass_word = passsword
                vall.etat_password = True
                Sessionalchemy.commit()
                flash("Vous avez changé avec succés votre mot de pass, bienvenue dans votre compte Sen Ndawal!","succes") 
                if role == 'investisseur':
                    return redirect(url_for('chef'))
                else:
                    return redirect(url_for('caissier'))

            else:
                flash("Le mot de passe saisi est trés faible, augmentez le nombre au minimum 6 charactéres.","danger")
                return redirect(url_for('changepassword'))
        else:
            flash("Les mots de passes ne sont pas identiques!","danger") 
            return redirect(url_for('changepassword'))

    else:
        flash("désolé, vous ne pouvez pas accéder à ce compte", "danger")
        return redirect(url_for('login'))

    # return render_template('bonpassword.html')


@app.route('/optionn')
def optionn():
    a = request.args.get('a', "", type=str)
    result = Sessionalchemy.query(Djeun)\
        .filter(Djeun.type_commande==a)\
        .distinct()

    valu = []
    liste_res = []
    for i in result:   
        photo = i.photo
        nom = i.nom
        prix = i.prix
        id = i.id
        val = i.valeur
        old = i.old_prix
        valu.append((photo, nom, prix, id, val, old))
        
        # for val in result:
        mon_dict= { "categorie":nom, "prix":prix }
        liste_res.append(mon_dict)
    print(liste_res)
    return jsonify(liste_res)


    return redirect(url_for('caissier'))
    

@app.route('/newcommande', methods=['GET', 'POST'])
def newcommande():
    if session.get('role') == "caissier" and session.get('statut') == "actif":
        newcomm = []
        valider = "Non"
        if request.method == "POST": 
            datas = []
            carte = request.form.get("qqq")
            livrais = request.form['livrais']
            depense = request.form['dep']
            provenance = "enregistrer"
            # print(carte)
            # Nettoyage des données
            carte1 = carte.replace("[", "")
            carte2 = carte1.replace("]", "")
            carte3 = carte2.split("},")
            s = carte3[-1].replace("}", "")
            carte3[-1] = s
            
            for j in carte3:
                val1 = j+"}"
                val2 = json.loads(val1)
                datas.append(val2)
                session['data'] = datas
            # print(datas, "***", livrais)

            idss = session.get('id')
            ids = Sessionalchemy.query(User).filter_by(id=idss).first()
            datelivraison = "Déja livré"
            fraislivraison = livrais
            addlivraison = "néant"
            prenom = "néant"
            nom = "néant"
            telephoneq = "néant"
            alphabets = string.ascii_letters + string.digits
            vals = generate_password_hash(alphabets)
            vals2 = vals[-10:]

            appeller = "OUI"
            livrer = "OUI"
            now = datetime.datetime.now()
            date_commande = now.strftime("%Y-%m-%d %H:%M:%S")
            boutique = ""
            idcom = Sessionalchemy.query(User.boutique_id)\
                    .filter(User.id==idss)\
                    .one() 
            commaqx = Sessionalchemy.query(Boutiques).filter_by(id=idcom).first()
            
            # print(fraislivraison)
            if datas == None:
                return redirect(url_for('catalog'))
            else:
                tots = 0
                for i in datas:
                    total = i['val5']
                    tots = tots + int(total)
                dep = int(depense)
                benef = tots + int(livrais) - dep
                comm = Commandes(prenom, nom, telephoneq, date_commande,tots, fraislivraison, addlivraison,datelivraison,appeller,livrer, provenance,valider,dep,benef, vals2, ids, commaqx)
                Sessionalchemy.add(comm)
                Sessionalchemy.commit()


                tas = 0
                for i in datas:
                    produit = i['val2']
                    prix = i['val3']
                    qty = i['val4']
                    total = i['val5']
                    tas = tas + int(total)
                    photoo = Sessionalchemy.query(Djeun.photo)\
                            .filter(Djeun.nom==produit)\
                            .first() 
                    categoriew = Sessionalchemy.query(Djeun.type_commande)\
                            .filter(Djeun.nom==produit)\
                            .first()  
                    
                    idcom = Sessionalchemy.query(Commandes.id)\
                            .filter(Commandes.num_client==telephoneq)\
                            .filter(Commandes.date_commande==date_commande)\
                            .filter(Commandes.valunique==vals2)\
                            .first() 
                    comma = Sessionalchemy.query(Commandes).filter_by(id=idcom[0]).first()
                    prod = Produits(produit, photoo, prix, qty, total,fraislivraison,tas, comma, categoriew)
                    Sessionalchemy.add(prod)
                    Sessionalchemy.commit()
                    flash("Félicitation, une nouvelle commande est enregistrée avec succé", "succes") 
            return redirect(url_for('caissier'))
    else:
        flash("désolé, vous ne pouvez pas accéder à ce compte", "danger")
        return redirect(url_for('login'))


@app.route('/newcommandecli', methods=['GET', 'POST'])
def newcommandecli():
    if session.get('role') == "caissier" and session.get('statut') == "actif":
        newcomm = []
        valider = "Non"
       

        if request.method == "POST": 
            datas = []
            carte = request.form.get("ttt")
            depense = request.form['dep']
            livrais = request.form['livrais']            
            prenmom = request.form['prenom']
            telephon = request.form['telephon']
            adress = request.form['adress']
            provenance = "enregistrer"
            # Nettoyage des données
            carte1 = carte.replace("[", "")
            carte2 = carte1.replace("]", "")
            carte3 = carte2.split("},")
            s = carte3[-1].replace("}", "")
            carte3[-1] = s
            
            for j in carte3:
                val1 = j+"}"
                val2 = json.loads(val1)
                datas.append(val2)
                session['data'] = datas
            # print(datas, "*", prenom, "*", telephon,"*", adress)

            idss = session.get('id')
            ids = Sessionalchemy.query(User).filter_by(id=idss).first()
            datelivraison = "Déja livré"
            fraislivraison = livrais
            addlivraison = adress
            prenom = prenmom
            nom = "néant"
            telephoneq = telephon
            alphabets = string.ascii_letters + string.digits
            vals = generate_password_hash(alphabets)
            vals2 = vals[-10:]

            appeller = "OUI"
            livrer = "OUI"
            now = datetime.datetime.now()
            date_commande = now.strftime("%Y-%m-%d %H:%M:%S")
            idcom = Sessionalchemy.query(User.boutique_id)\
                    .filter(User.id==idss)\
                    .one() 
            commaqw = Sessionalchemy.query(Boutiques).filter_by(id=idcom).first()
            
            # print(fraislivraison)
            if datas == None:
                return redirect(url_for('catalog'))
            else:
                tots = 0
                for i in datas:
                    total = i['val5']
                    tots = tots + int(total)
                dep = int(depense)
                benef = tots + int(livrais) - dep
                comm = Commandes(prenom, nom, telephoneq, date_commande,tots, fraislivraison, addlivraison,datelivraison,appeller,livrer, provenance, valider,dep, benef, vals2, ids, commaqw)
                Sessionalchemy.add(comm)
                Sessionalchemy.commit()


                tas = 0
                for i in datas:
                    produit = i['val2']
                    prix = i['val3']
                    qty = i['val4']
                    total = i['val5']
                    tas = tas + int(total)
                    photoo = Sessionalchemy.query(Djeun.photo)\
                            .filter(Djeun.nom==produit)\
                            .first()  
                    categoriex = Sessionalchemy.query(Djeun.type_commande)\
                            .filter(Djeun.nom==produit)\
                            .first()  
                    
                    idcom = Sessionalchemy.query(Commandes.id)\
                            .filter(Commandes.num_client==telephoneq)\
                            .filter(Commandes.date_commande==date_commande)\
                            .filter(Commandes.valunique==vals2)\
                            .first() 
                    comma = Sessionalchemy.query(Commandes).filter_by(id=idcom[0]).first()
                    prod = Produits(produit, photoo, prix, qty, total,fraislivraison,tas, comma, categoriex)
                    Sessionalchemy.add(prod)
                    Sessionalchemy.commit()
                    flash("Félicitation, une nouvelle commande est enregistrée avec succé", "succes")
            return redirect(url_for('caissier'))

    else:
        flash("désolé, vous ne pouvez pas accéder à ce compte", "danger")
        return redirect(url_for('login'))


@app.route('/commandeenligne', methods=['GET', 'POST'])
def commandeenligne():
    if session.get('id') != None and session.get('role') == "caissier":
        id_user = session.get('id',None)
        lst = []
        newcommm = session.get('newcomm')
        lst.append(newcommm)

        moi = []
        liste = []
        new_commandes = []
        new_commande = []

        Commandesss = Sessionalchemy.query(Commandes)\
            .filter(Commandes.livrer=='NON')\
            .order_by(Commandes.date_commande)\
            .all()
        for i in Commandesss:
            id = i.id
            prenomcl = i.prenom_client
            nomcl = i.nom_client
            numcl = i.num_client
            datecomm = i.date_commande
            fraislivraison = i.frais_livraison
            adresslivraison = i.adresslivraison
            datealiver = i.date_a_livrer
            appeler = i.appeller
            livrer = i.livrer
            tots = i.total
            new_commandes.append((id, prenomcl, nomcl, numcl, datecomm, fraislivraison, adresslivraison, datealiver, appeler,livrer, tots))
        
        me = Sessionalchemy.query(User)\
            .filter(User.id==id_user)\
            .all()
        for i in me:
            prenme = i.prenom
            nomme = i.nom
            moi.append((prenme,nomme))
    else:
        flash("désolé, vous ne pouvez pas accéder à ce compte", "danger")
        return redirect(url_for('login')) 

    return render_template('commandeenligne.html', lst=lst, new_commandes=new_commandes, moi=moi)

@app.route('/valid/<string:idap>', methods=['GET'])
def valid(idap):
    if session.get('role') == "comptable" and session.get('statut') == "actif":
        dt = datetime.datetime.today()
        todays_datetime = datetime.datetime(dt.year, dt.month, dt.day)
        commandday = Sessionalchemy.query(Commandes)\
            .filter(Commandes.user_id == idap)\
            .filter(Commandes.valider == "Non")\
            .all()
        
        for val in commandday:
            val.valider = 'OUI'
        Sessionalchemy.commit()
        flash("Vous avez validé les commandes enregistrées par le caissier aujourd'hui.","succes")
        return redirect(url_for('comptable'))

    else:
        flash("désolé, vous ne pouvez pas accéder à ce compte", "danger")
        return redirect(url_for('login'))

@app.route('/depenses', methods=['GET', 'POST'])
def depenses():
    if session.get('role') == "comptable" and session.get('statut') == "actif":
        idss = session.get('id')
        categorie = request.form['options']
        produit1 = request.form['produit']  
        produit = produit1.replace("_", " ")
        montant = request.form['montant']
        nombre = request.form['nombre']
        justification = request.form['Comment']
        boutique = request.form['boutique']
        transport = request.form['transport']
        idsbout = Sessionalchemy.query(Boutiques).filter_by(nomboutique=boutique).first()
        
        totaldep = int(montant)*int(nombre)
        now = datetime.datetime.now()
        date_commande = now.strftime("%Y-%m-%d %H:%M:%S")
        ids = Sessionalchemy.query(User).filter_by(id=idss).first()
        dep = Depenses(categorie,produit, montant, nombre, totaldep, date_commande,transport, justification, idsbout, ids)
        Sessionalchemy.add(dep)
        Sessionalchemy.commit()

        obj = Sessionalchemy.query(Stockages)\
            .filter(Stockages.boutique == boutique)\
            .filter(Stockages.categorieprod == categorie)\
            .filter(Stockages.nomprod == produit)\
            .filter(Stockages.statut == "encours")\
            .order_by(Stockages.id.desc()).first()
        users = Sessionalchemy.query(User)\
                .filter(User.id == idss)\
                .first()
        typesstockage = "achat"
        statutstockage = ""
        commandeq = 0
        benef = 0
        punitachat = int(montant)
        punitvente = 0
        utilisateur = users.prenom + " " + users.nom
        qty = 0
        if obj != None:
            if int(obj.puachat) == punitachat:
                qty = int(obj.quantitestocker) + int(nombre)
                statutstockage = "encours"
                obj.statut = "encoursold"
            else:
                obj2 = Sessionalchemy.query(Stockages)\
                    .filter(Stockages.boutique == boutique)\
                    .filter(Stockages.categorieprod == categorie)\
                    .filter(Stockages.nomprod == produit)\
                    .filter(Stockages.statut == "enattente")\
                    .order_by(Stockages.id.desc()).first()
                if obj2 != None:
                    statutstockage = "enattente"
                    if  int(obj2.puachat) == punitachat:
                        qty = int(obj2.quantitestocker) + int(nombre)
                        obj2.statut = "enattenteold"
                    else:
                        qty = int(nombre)
                else:
                    qty = int(nombre)
                    statutstockage = "enattente"
        else:
            qty = int(nombre)
            statutstockage = "encours"
        stock = Stockages(boutique,qty, categorie, produit, punitachat, punitvente, benef, commandeq,utilisateur, typesstockage, statutstockage, date_commande)
        Sessionalchemy.add(stock)
        Sessionalchemy.commit()

        flash("Vous avez bien enregistré une dépense", "succes")
        return redirect(url_for('comptable'))
    
    else:
        flash("désolé, vous ne pouvez pas accéder à ce compte", "danger")
        return redirect(url_for('login'))
@app.route('/toutesdepenses', methods=['GET', 'POST'])
def toutesdepenses():
    if session.get('role') == "comptable" and session.get('statut') == "actif":
        dep = Sessionalchemy.query(Depenses)\
            .order_by(Depenses.date)\
            .all()
        depens = []
        for i in dep:
            montant = i.montant
            nbr = i.nombre
            tot = i.totaldepense
            datee = i.date
            justif = i.justification
            depens.append((montant, nbr, tot, datee, justif))
    
    else:
        flash("désolé, vous ne pouvez pas accéder à ce compte", "danger")
        return redirect(url_for('login'))

    return render_template('toutesdepenses.html', depens=depens)

    

@app.route('/visualcompt', methods=['GET', 'POST'])
def visualcompt():
    if session.get('role') == "comptable" and session.get('statut') == "actif":
        
        commandtot = Sessionalchemy.query(Commandes)\
            .filter(Commandes.valider == "OUI")\
            .all()
        
        totalcommandevalid = []
        commandetotale = []
        to = 0
        vis = []
        for i in commandtot:
            id = i.id
            prenomcl = i.prenom_client
            nomcl = i.nom_client
            numcl = i.num_client
            datecomm = i.date_commande
            fraislivraison = i.frais_livraison
            adresslivraison = i.adresslivraison
            datealiver = i.date_a_livrer
            appeler = i.appeller
            livrer = i.livrer
            tots = int(i.total) + int(fraislivraison)
            prov = i.provenance
            depense = i.depense
            benef = i.benefice
            to = to + tots
            commandetotale.append(to)
            totalcommandevalid.append((id, prenomcl, nomcl, numcl, datecomm, fraislivraison, adresslivraison, datealiver, appeler,livrer, tots, prov, depense, benef))
            vis.append((datecomm, tots, depense, benef))
        val = data_by_team1(vis)
        myvalue= []
        for j in val:
            myvalue.append((j,val[j][0],val[j][1], val[j][2], val[j][3]))
       
    else:
        flash("désolé, vous ne pouvez pas accéder à ce compte sans autorisation", "danger")
        return redirect(url_for('login'))

    return render_template('visualcompt.html', myvalue=myvalue)

@app.route('/ttecommandes/<string:idap>', methods=['GET'])
def ttecommandes(idap):
    if session.get('id'): 
        commandday = Sessionalchemy.query(Commandes)\
            .filter(Commandes.user_id == idap)\
            .filter(Commandes.valider == "OUI")\
            .all()
        
        toute = []
        mytotal = []
        to = 0
        vis = []
        for i in commandday:
            id = i.id
            prenomcl = i.prenom_client
            nomcl = i.nom_client
            numcl = i.num_client
            datecomm = i.date_commande
            fraislivraison = i.frais_livraison
            adresslivraison = i.adresslivraison
            datealiver = i.date_a_livrer
            appeler = i.appeller
            livrer = i.livrer
            tots = int(i.total) + int(fraislivraison)
            prov = i.provenance
            depense = i.depense
            benef = i.benefice
            to = to + tots
            mytotal.append((datecomm, to, prenomcl, nomcl))
            toute.append((id, prenomcl, nomcl, numcl, datecomm, fraislivraison, adresslivraison, datealiver, appeler,livrer, tots, prov, depense, benef))
            tots = int(i.total) + int(fraislivraison)
            vis.append((datecomm, tots, depense, benef))
        val = data_by_team1(vis)
        myvalue= []
        for j in val:
            myvalue.append((j,val[j][0],val[j][1], val[j][2], val[j][3]))
    
    else:
        flash("désolé, vous ne pouvez pas accéder à ce compte", "danger")
        return redirect(url_for('login'))

    return render_template('ttecommandes.html', mytotal=mytotal, toute=toute, myvalue=myvalue)


@app.route('/ttemycommandes/<string:idap>', methods=['GET'])
def ttemycommandes(idap):
    if session.get('id'): 
        commandday = Sessionalchemy.query(Commandes)\
            .filter(Commandes.user_id == idap)\
            .filter(Commandes.valider == "OUI")\
            .all()
        
        toute = []
        mytotal = []
        to = 0
        vis = []
        for i in commandday:
            id = i.id
            prenomcl = i.prenom_client
            nomcl = i.nom_client
            numcl = i.num_client
            datecomm = i.date_commande
            fraislivraison = i.frais_livraison
            adresslivraison = i.adresslivraison
            datealiver = i.date_a_livrer
            appeler = i.appeller
            livrer = i.livrer
            tots = int(i.total) + int(fraislivraison)
            prov = i.provenance
            depense = i.depense
            benef = i.benefice
            to = to + tots
            mytotal.append((datecomm, to, prenomcl, nomcl))
            toute.append((id, prenomcl, nomcl, numcl, datecomm, fraislivraison, adresslivraison, datealiver, appeler,livrer, tots, prov))
            tots = int(i.total) + int(fraislivraison)
            vis.append((datecomm, tots, depense, benef))
            
        val = data_by_team1(vis)
        myvalue= []
        for j in val:
            myvalue.append((j,val[j][0],val[j][1],val[j][2],val[j][3]))
        
        
    
    else:
        flash("désolé, vous ne pouvez pas accéder à ce compte sans autorisation", "danger")
        return redirect(url_for('login'))

    return render_template('ttemycommandes.html', mytotal=mytotal, toute=toute, myvalue=myvalue)


@app.route('/markting', methods=['GET', 'POST'])
def markting():
    if session.get('role') == "markting" and session.get('statut') == "actif":
        id_user = session.get('id',None)
        moi = []
        me = Sessionalchemy.query(User)\
            .filter(User.id==id_user)\
            .all()
        for i in me:
            prenme = i.prenom
            nomme = i.nom
            moi.append((prenme,nomme))


        commandday = Sessionalchemy.query(Commandes)\
            .filter(Commandes.num_client != "néant")\
            .filter(Commandes.valider == "OUI")\
            .order_by(Commandes.date_commande)\
            .all()
        
        to = 0
        vis = []
        total = []
        for i in commandday:
            id = i.id
            datecomm = i.date_commande
            fraislivraison = i.frais_livraison
            adresslivraison = i.adresslivraison
            tots = int(i.total) + int(fraislivraison)
            prov = i.provenance
            to = to + tots
            total.append(to)
            vis.append((datecomm, tots))
        
        df = pd.DataFrame([(d.date_commande, d.prenom_client, d.nom_client, d.num_client, d.total, d.frais_livraison, d.adresslivraison, d.date_a_livrer, d.provenance) for d in commandday], 
                  columns=['date_commande', 'prenom_client', 'nom_client', 'num_client', 'total_commande_produit', 'fraislivraison', 'adresslivraison', 'date_a_livrer', 'provenance'])
        df[["total_commande_produit", "fraislivraison"]] = df[["total_commande_produit", "fraislivraison"]].apply(pd.to_numeric)  # convertir colonne en numeric
        sum_column = df["total_commande_produit"] + df["fraislivraison"]
        df["revenus_total_commande"] = sum_column   # créer une nouvelle colonne pour le revenu total
        
        df['date_commande'] = pd.to_datetime(df['date_commande'])  # convertir en date la colonne
        revenus_mois = df.groupby(df['date_commande'].dt.strftime('%B'))['revenus_total_commande'].sum().sort_values()
        revenus_nbr_mois = df.groupby(df['date_commande'].dt.strftime('%B'))['revenus_total_commande'].count()

        revenus = revenus_mois.index.tolist()

        # lieu_proprietaire = df.groupby(df['date_commande'].dt.strftime('%B'), 'num_client')['revenus_total_commande'].sum().nlargest(3)
        # print(lieu_proprietaire)
        montant_mois = [d for d in revenus_mois]
        nbr = [d for d in revenus_nbr_mois]
        mois = [revenus, montant_mois, nbr]  #
        # largest_states = lieu_proprietaire.index.tolist()
            
        val = data_by_team(vis)
        myvalue= []
        for j in val:
            myvalue.append((j,val[j][0],val[j][1]))
        
    else:
        flash("désolé, vous ne pouvez pas accéder à ce compte", "danger")
        return redirect(url_for('login'))

    return render_template('markting.html', myvalue=myvalue, moi=moi, total=total, mois=mois)

@app.route('/marktingclient', methods=['GET', 'POST'])
def marktingclient():
    if session.get('role') == "markting" and session.get('statut') == "actif":
        page = int(request.args.get('page', 1))
        per_page = 15
        offset = (page - 1) * per_page
        id_user = session.get('id',None)
        moi = []
        me = Sessionalchemy.query(User)\
            .filter(User.id==id_user)\
            .all()
        for i in me:
            prenme = i.prenom
            nomme = i.nom
            moi.append((prenme,nomme))

        mescommandes = Sessionalchemy.query(Commandes)\
            .distinct(Commandes.num_client)\
            .filter(Commandes.num_client != "néant")\
            .filter(Commandes.valider == "OUI")\
            .all()

        new_commande = []
        vis = []
        for i in mescommandes:
            id = i.id
            prenomcl = i.prenom_client
            nomcl = i.nom_client
            numcl = i.num_client
            adresslivraison = i.adresslivraison
            new_commande.append((id, prenomcl, nomcl, numcl, adresslivraison))
    


        infclient = Sessionalchemy.query(Commandes)\
            .filter(Commandes.num_client != "néant")\
            .all()

        df = pd.DataFrame([(d.date_commande, d.prenom_client, d.nom_client, d.num_client, d.total, d.frais_livraison, d.adresslivraison, d.date_a_livrer, d.provenance) for d in infclient], 
                  columns=['date_commande', 'prenom_client', 'nom_client', 'num_client', 'total_commande_produit', 'fraislivraison', 'adresslivraison', 'date_a_livrer', 'provenance'])
        df[["total_commande_produit", "fraislivraison"]] = df[["total_commande_produit", "fraislivraison"]].apply(pd.to_numeric)  # convertir colonne en numeric
        sum_column = df["total_commande_produit"] + df["fraislivraison"]
        df["revenus_total_commande"] = sum_column   # créer une nouvelle colonne pour le revenu total
        
        df['date_commande'] = pd.to_datetime(df['date_commande'])  # convertir en date la colonne
        num_revenu = df.groupby('num_client')['revenus_total_commande'].sum().sort_values()
        nums = num_revenu.index.tolist()
        num_revenus_tot = [d for d in num_revenu]
        tous = [nums, num_revenus_tot]
        num_frequance = df.groupby('num_client')['num_client'].count().sort_values()
        nums_freq = num_frequance.index.tolist()
        num_freq_tot = [d for d in num_frequance]
        frequance = [nums_freq, num_freq_tot]

    else:
        flash("désolé, vous ne pouvez pas accéder à ce compte", "danger")
        return redirect(url_for('login'))

    total = len(new_commande)
    paginationUsers = new_commande[offset: offset + per_page]
    pagination = Pagination(page=page, per_page=per_page, total=total,
                            css_framework='bootstrap4')
  

    return render_template('marktingclient.html', moi=moi, tous=tous, frequance=frequance, users=paginationUsers, page=page, per_page=per_page,pagination=pagination)

@app.route('/detailclient/<string:idap>', methods=['GET'])
def detailclient(idap):
    print(idap)
    if session.get('role') == "markting" and session.get('statut') == "actif":
        infclient = Sessionalchemy.query(Commandes)\
            .filter(Commandes.num_client == idap)\
            .all()
        df = pd.DataFrame([(d.date_commande, d.prenom_client, d.nom_client, d.num_client, d.total, d.frais_livraison, d.adresslivraison, d.date_a_livrer, d.provenance) for d in infclient], 
                  columns=['date_commande', 'prenom_client', 'nom_client', 'num_client', 'total_commande_produit', 'fraislivraison', 'adresslivraison', 'date_a_livrer', 'provenance'])
        df[["total_commande_produit", "fraislivraison"]] = df[["total_commande_produit", "fraislivraison"]].apply(pd.to_numeric)  # convertir colonne en numeric
        sum_column = df["total_commande_produit"] + df["fraislivraison"]
        df["revenus_total_commande"] = sum_column   # créer une nouvelle colonne pour le revenu total
        
        df['date_commande'] = pd.to_datetime(df['date_commande'])  # convertir en date la colonne
        client_mois_achat = df.groupby(df['date_commande'].dt.strftime('%B'))['revenus_total_commande'].sum().sort_values()
        mois = client_mois_achat.index.tolist()
        achats = [d for d in client_mois_achat]
        tous = [mois, achats]


        
        ids = Sessionalchemy.query(Commandes).filter(Commandes.num_client==idap).all()
        idss = []
        details = []
        moi = []
        # myclient = []
        # tt = 0
        for i in ids:
            idss.append(i.id)

            dets = Sessionalchemy.query(Produits)\
                .filter(Produits.commandes_id==i.id)\
                .all()
            for j in dets:
                details.append(j.nom)
           
        vv = Counter(details).most_common()
            
        val = data_by_team(vv)
        myvalue= []
        for j in val:
            myvalue.append((j,val[j][0],val[j][1]))
        
        id_user = session.get('id',None)
        me = Sessionalchemy.query(User)\
            .filter(User.id==id_user)\
            .all()
        for i in me:
            prenme = i.prenom
            nomme = i.nom
            moi.append((prenme,nomme))
    else:
        flash("désolé, vous ne pouvez pas accéder à ce compte", "danger")
        return redirect(url_for('login'))
    
    return render_template('detailclient.html', tous=tous, moi=moi, myvalue=myvalue)

@app.route('/chefcomptable', methods=['GET', 'POST'])
def chefcomptable():
    if request.method == 'POST':

        prenomc = request.form['prenomc']
        nomc = request.form['nomc']
        datenaissc = request.form['datenaissc']
        adressec = request.form['adressec']
        telephonec = request.form['telephonec']
        cnic = request.form['cnic']
        mailc = request.form['mailc']
        motpass = request.form['motpass']
        salairec = request.form['salairec']
        rolec = "comptable"
        codec = ""
        pass_wordx = generate_password_hash(motpass)

        exitscni = Sessionalchemy.query(User.id).filter(User.cni==cnic).filter(User.statut=="actif").scalar()
        exitsnum = Sessionalchemy.query(User.id).filter(User.telephone==telephonec).filter(User.statut=="actif").scalar()
        exitscomptable = Sessionalchemy.query(User.id).filter(User.role=="comptable").filter(User.statut=="actif").scalar()
        if exitscomptable != None:
            flash("Attention: merci de supprimer d'abord le compte du chef comptable avant d'en créer un autre", "danger")
            return redirect(url_for('chef')) 
        else:
            if exitsnum != None:
                flash("Attention: Un compte est déja enregistré avec ce numéro", "danger")
                return redirect(url_for('chef'))   
            else:
                if exitscni != None: 
                    flash("Attention: Un compte est déja enregistrée avec cette CNI", "danger")
                    return redirect(url_for('chef'))
                else:
                    valls = Sessionalchemy.query(User)\
                        .filter(User.role=="comptable")\
                        .filter(User.statut=="supprimer")\
                        .all()
                    for val in valls:
                        val.pass_word = pass_wordx
                        val.prenom = prenomc
                        val.nom = nomc
                        val.telephone = telephonec
                        val.mail = mailc
                        val.adresse = adressec
                        val.cni = cnic
                        val.datenaissance = datenaissc
                        val.etat_password = "true"
                        val.statut = "actif"
                        val.salaire = salairec
                    Sessionalchemy.commit()
                    flash("Felicitation, vous avez créer un chef comptable", "succes")
                    return redirect(url_for('chef'))
    else:
        flash("désolé, vous ne pouvez pas accéder à ce compte", "danger")
        return redirect(url_for('login'))

@app.route('/supprimcomptable', methods=['GET', 'POST'])
def supprimcomptable():
    if request.method == 'POST':
        phonesupp = request.form['phonesupp']
        cnisupp = request.form['cnisupp']
        exitscniphon = Sessionalchemy.query(User.id)\
            .filter(User.cni==cnisupp)\
            .filter(User.telephone==phonesupp)\
            .scalar()
        rolesupp = Sessionalchemy.query(User.role)\
            .filter(User.cni==cnisupp)\
            .filter(User.telephone==phonesupp)\
            .first()
        if exitscniphon != None and rolesupp[0] == "comptable":
            alphabet = string.ascii_letters + string.digits
            val = generate_password_hash(alphabet)
            val2 = val[-4:]
            val = Sessionalchemy.query(User)\
                .filter(User.cni==cnisupp)\
                .filter(User.telephone==phonesupp)\
                .first()
            val.statut = "supprimer"
            val.etat_password = "false"
            val.telephone = phonesupp+"SupprimeR"+val2
            val.cni = cnisupp+"SupprimeR"+val2
            Sessionalchemy.commit()
            flash("Vous avez supprimé le chef comptable, merci d'en créer un autre", "danger")
            return redirect(url_for('chef'))
        else:  
            flash("Attention: revoyez les infos renseignées svp", "danger")
            return redirect(url_for('chef')) 
    else:
        return redirect(url_for('chef'))

@app.route('/chefmarkting', methods=['GET', 'POST'])
def chefmarkting():
    if request.method == 'POST':
        prenomc = request.form['prenomc']
        nomc = request.form['nomc']
        datenaissc = request.form['datenaissc']
        adressec = request.form['adressec']
        telephonec = request.form['telephonec']
        cnic = request.form['cnic']
        mailc = request.form['mailc']
        motpass = request.form['motpass']
        salairec = request.form['salairec']
        rolec = "markting"
        codec = ""
        pass_wordx = generate_password_hash(motpass)

        exitscni = Sessionalchemy.query(User.id).filter(User.cni==cnic).filter(User.statut=="actif").scalar()
        exitsnum = Sessionalchemy.query(User.id).filter(User.telephone==telephonec).filter(User.statut=="actif").scalar()
        exitsmarkting = Sessionalchemy.query(User.id).filter(User.role=="markting").filter(User.statut=="actif").scalar()
        if exitsmarkting != None:
            flash("Attention: merci de supprimer d'abord le compte du responsable markting avant d'en créer un autre", "danger")
            return redirect(url_for('chef')) 
        else:
            if exitsnum != None:
                flash("Attention: Un compte est déja enregistré avec ce numéro", "danger")
                return redirect(url_for('chef'))   
            else:
                if exitscni != None: 
                    flash("Attention: Un compte est déja enregistrée avec cette CNI", "danger")
                    return redirect(url_for('chef'))
                else:
                    valls = Sessionalchemy.query(User)\
                        .filter(User.role=="markting")\
                        .filter(User.statut=="supprimer")\
                        .all()
                        
                    for val in valls:
                        val.pass_word = pass_wordx
                        val.prenom = prenomc
                        val.nom = nomc
                        val.telephone = telephonec
                        val.mail = mailc
                        val.adresse = adressec
                        val.cni = cnic
                        val.datenaissance = datenaissc
                        val.etat_password = "true"
                        val.statut = "actif"
                        val.salaire = salairec
                    Sessionalchemy.commit()
                    flash("Felicitation, vous avez créer le responsable markting", "succes")
                    return redirect(url_for('chef'))
    else:
        flash("désolé, vous ne pouvez pas accéder à ce compte", "danger")
        return redirect(url_for('login'))

@app.route('/chefinvestisseur', methods=['GET', 'POST'])
def chefinvestisseur():
    if request.method == 'POST':
        prenomc = request.form['prenomc']
        nomc = request.form['nomc']
        datenaissc = request.form['datenaissc']
        adressec = request.form['adressec']
        telephonec = request.form['telephonec']
        cnic = request.form['cnic']
        mailc = request.form['mailc']
        motpass = request.form['motpass']
        montant = request.form['montantc']
        pourcentagec = request.form['pourcentagec']
        conseil_admin = request.form['conseiladminc']
        rolec = "investisseur"
        codec = ""
        region = ""
        etat_pwd = "false"
        statut = "actif"
        salaire = ""
        
        valinvest = int(montant)/500000
        if valinvest != int(pourcentagec):
            flash("Attention: Le montant enregistré n'est pas équivalent au pourcentage renseigné, revoyez les informations", "danger")
            return redirect(url_for('chef'))
        else:
            # pass_wordx = generate_password_hash(motpass)
            # lettres min + lettres maj + chiffres
            pop = string.ascii_letters + string.digits
            # le mot de passe fera 6 caractères de long
            k = 6

            # sample retourne une portion aléatoire et de taille k à partir de la séquence pop
            passwd = ''.join(sample(pop, k))
            passc = generate_password_hash(passwd) #hachage password

            exitscni = Sessionalchemy.query(User.id).filter(User.cni==cnic).filter(User.statut=="actif").scalar()
            exitsnum = Sessionalchemy.query(User.id).filter(User.telephone==telephonec).filter(User.statut=="actif").scalar()
            
            if exitsnum != None:
                flash("Attention: Un compte est déja enregistré avec ce numéro", "danger")
                return redirect(url_for('chef'))   
            else:
                if exitscni != None: 
                    flash("Attention: Un compte est déja enregistrée avec cette CNI", "danger")
                    return redirect(url_for('chef'))
                else:
                    import smtplib
                    from smtplib import SMTPException

                    fromaddr = 'senndawal2020@gmail.com'
                    toaddrs  = mailc
                    nomm = prenomc + " " + nomc
                    
                    msg = "\r\n".join([
                        "From: senndawal2020@gmail.com",
                        "To: {}".format(mailc),
                        "Subject: Activation de compte",
                        "",
                        "Bonjour {} ".format(nomm) + "votre mot de pass par defaut est: {}".format(passwd)
                        ])
                    username = 'senndawal2020@gmail.com'
                    password = 's@nnd@w@lMNK'
                    server = smtplib.SMTP('smtp.gmail.com:587')
                    server.ehlo()
                    server.starttls()
                    server.login(username,password)
                    server.sendmail(fromaddr, toaddrs, msg)
                    server.quit()
                    pourcentage = pourcentagec+"%"
                    boutinvest = ""
                    # bouts = Sessionalchemy.query(User.id)\
                    # .filter(User.nomboutique==boutique)\
                    # .one()
                    idsbout = Sessionalchemy.query(Boutiques).filter_by(nomboutique=boutinvest).first()
                    newinvestisseur = User(prenomc, nomc, telephonec, mailc, passc, adressec, region, codec, cnic, datenaissc, rolec, etat_pwd, statut, pourcentage, montant, salaire, conseil_admin, idsbout)
                    Sessionalchemy.add(newinvestisseur)
                    Sessionalchemy.commit()
                    
                    flash("Felicitation, vous avez créer un nouveau investisseur du nom de {}".format(prenomc) +" {}".format(nomc), "succes")
                    return redirect(url_for('chef'))
    else:
        flash("désolé, vous ne pouvez pas accéder à ce compte", "danger")
        return redirect(url_for('login'))

@app.route('/suppriminvestisseur', methods=['GET', 'POST'])
def suppriminvestisseur():
    if request.method == 'POST':
        phonesupp = request.form['phonesupp']
        cnisupp = request.form['cnisupp']
        exitscniphon = Sessionalchemy.query(User.id)\
            .filter(User.cni==cnisupp)\
            .filter(User.telephone==phonesupp)\
            .scalar()
        rolesupp = Sessionalchemy.query(User.role)\
            .filter(User.cni==cnisupp)\
            .filter(User.telephone==phonesupp)\
            .first()
        if exitscniphon != None and rolesupp[0] == "investisseur":
            alphabet = string.ascii_letters + string.digits
            val = generate_password_hash(alphabet)
            val2 = val[-4:]
            val = Sessionalchemy.query(User)\
                .filter(User.cni==cnisupp)\
                .filter(User.telephone==phonesupp)\
                .first()
            val.statut = "supprimer"
            val.etat_password = "false"
            val.telephone = phonesupp+"SupprimeR"+val2
            val.cni = cnisupp+"SupprimeR"+val2
            Sessionalchemy.commit()
            flash("Vous avez supprimé l'investisseur de téléphone {}".format(phonesupp) +" et de CNI {}".format(cnisupp), "danger")
            return redirect(url_for('chef'))
        else:  
            flash("Attention: revoyez les infos renseignées svp", "danger")
            return redirect(url_for('chef')) 
    else:
        return redirect(url_for('chef'))    




@app.route('/supprimmarkting', methods=['GET', 'POST'])
def supprimmarkting():
    if request.method == 'POST':
        phonesupp = request.form['phonesupp']
        cnisupp = request.form['cnisupp']
        exitscniphon = Sessionalchemy.query(User.id)\
            .filter(User.cni==cnisupp)\
            .filter(User.telephone==phonesupp)\
            .scalar()
        rolesupp = Sessionalchemy.query(User.role)\
            .filter(User.cni==cnisupp)\
            .filter(User.telephone==phonesupp)\
            .first()
        if exitscniphon != None and rolesupp[0] == "markting":
            alphabet = string.ascii_letters + string.digits
            val = generate_password_hash(alphabet)
            val2 = val[-4:]
            val = Sessionalchemy.query(User)\
                .filter(User.cni==cnisupp)\
                .filter(User.telephone==phonesupp)\
                .first()
            val.statut = "supprimer"
            val.etat_password = "false"
            val.telephone = phonesupp+"SupprimeR"+val2
            val.cni = cnisupp+"SupprimeR"+val2
            Sessionalchemy.commit()
            flash("Vous avez supprimé le responsable markting, merci d'en créer un autre", "danger")
            return redirect(url_for('chef'))

            
        else:  
            flash("Attention: revoyez les infos renseignées svp", "danger")
            return redirect(url_for('chef')) 
    else:
        return redirect(url_for('chef')) 

@app.route('/supprimcaissier', methods=['GET', 'POST'])
def supprimcaissier():
    if request.method == 'POST':
        phonesupp = request.form['phonesupp']
        cnisupp = request.form['cnisupp']
        exitscniphon = Sessionalchemy.query(User.id)\
            .filter(User.cni==cnisupp)\
            .filter(User.telephone==phonesupp)\
            .scalar()
        rolesupp = Sessionalchemy.query(User.role)\
            .filter(User.cni==cnisupp)\
            .filter(User.telephone==phonesupp)\
            .first()
        if exitscniphon != None and rolesupp[0] == "caissier":
            alphabet = string.ascii_letters + string.digits
            val = generate_password_hash(alphabet)
            val2 = val[-4:]
            val = Sessionalchemy.query(User)\
                .filter(User.cni==cnisupp)\
                .filter(User.telephone==phonesupp)\
                .first()
            val.statut = "supprimer"
            val.etat_password = "false"
            val.telephone = phonesupp+"SupprimeR"+val2
            val.cni = cnisupp+"SupprimeR"+val2
            Sessionalchemy.commit()
            flash("Vous avez supprimé un caissier", "danger")
            return redirect(url_for('chef'))
        else:  
            flash("Attention: revoyez les infos renseignées svp", "danger")
            return redirect(url_for('chef')) 
    else:
        return redirect(url_for('chef'))    


##################  GOOGLE ANALYTICS   ############################
from googleanalytics import googleanalytics
@app.route('/googleanalytics', methods=['GET', 'POST'])
def ga():
    dfCity, dfTypeUser, dfDate, dfGeo = googleanalytics()

    CityListe1 = dfCity['ga:city'].tolist()
    userListe1 = dfCity['ga:users'].tolist()
    liste1 = [CityListe1,userListe1]

    usertypeListe2 = dfTypeUser['ga:userType'].tolist()
    userListe2 = dfTypeUser['ga:users'].tolist()
    liste2 = [usertypeListe2,userListe2]

    dateListe3 = dfDate['ga:date'].tolist()
    userListe3 = dfDate['ga:users'].tolist()
    liste3 = [dateListe3,userListe3]

    longitudeListe4 = dfGeo['ga:longitude'].tolist()
    latitudeListe4 = dfGeo['ga:latitude'].tolist()
    cityListe4 = dfGeo['ga:city'].tolist()
    liste4 = []
    for i in range(0,len(longitudeListe4)):
        print(longitudeListe4[i], latitudeListe4[i], cityListe4[i])
        liste4.append((longitudeListe4[i], latitudeListe4[i], cityListe4[i]))

    # print(dfCity)
    # print(dfTypeUser)
    # print(dfDate)
    # print(dfGeo)


    return render_template('ga.html', liste1=liste1, liste2=liste2, liste3=liste3, liste4=liste4)


#########################################  DECONNEXION  ###########################

@app.route('/deconnexion')
def logout():
    # Remove session data, this will log the user out
   session.pop('loggedin', None)
   session.pop('id', None)
   session.pop('nom_utilisateur', None)
   session.pop('role', None)
   # Redirect to login page
   return redirect(url_for('login'))

#    return render_template('deconnexion.html')

if __name__ == '__main__': #si le fichier est executer alors execute le bloc
    app.run(debug=True) #debug=True relance le serveur à chaque modification




#######################################################################################""
