# coding=utf-8

from sqlalchemy import Column, String, Integer, Numeric, Date, ForeignKey

from basepoissons import Base


class Message(Base):
    __tablename__ = 'message'

    id = Column(Integer, primary_key=True)
    nom = Column(String)
    prenom = Column(String)
    mail = Column(String)
    message = Column(String)
    

    def __init__(self, nom, prenom, mail, message):
        self.nom = nom
        self.prenom = prenom
        self.mail = mail
        self.message = message
        
