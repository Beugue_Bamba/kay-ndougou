# coding=utf-8

from sqlalchemy import Column, String, Integer, Numeric, Date, ForeignKey
from sqlalchemy.orm import relationship

from basepoissons import Base


class Depenses(Base):
    __tablename__ = 'depenses'

    id = Column(Integer, primary_key=True)
    categorie = Column(String)
    produits = Column(String)
    montant = Column(String)
    nombre = Column(Numeric)
    totaldepense = Column(String)
    date = Column(Date)
    fraistransport = Column(Numeric)
    justification = Column(String)
    boutique_id = Column(Integer, ForeignKey('boutique.id'))
    boutique = relationship("Boutiques", backref="boutiquess")
    user_id = Column(Integer, ForeignKey('users.id'))
    user = relationship("User", backref="modeless")
    


    def __init__(self,categorie , produits,  montant, nombre, totaldepense, date, fraistransport, justification, boutique, user):
        self.categorie = categorie
        self.produits = produits
        self.montant = montant
        self.nombre = nombre
        self.totaldepense = totaldepense
        self.date = date
        self.fraistransport = fraistransport
        self.justification = justification
        self.boutique = boutique
        self.user = user
        
