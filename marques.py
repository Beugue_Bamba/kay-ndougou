from sqlalchemy import Column, String, Integer, Numeric, Date

from basevoiture import Base


class Marques(Base):
    __tablename__ = 'marques'

    id = Column(Integer, primary_key=True)
    nom = Column(String)
    marque = Column(String)
    


    def __init__(self, nom, marque):
        self.nom = nom
        self.marque = marque
        