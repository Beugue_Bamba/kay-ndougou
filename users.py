# coding=utf-8

from sqlalchemy import Column, String, Integer, Date, ForeignKey
from sqlalchemy.orm import relationship


from basepoissons import Base


class User(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    prenom = Column(String)
    nom = Column(String)
    telephone = Column(String)
    mail = Column(String)
    pass_word = Column(String)
    adresse = Column(String)
    region = Column(String)
    code = Column(String)
    cni = Column(String)
    datenaissance = Column(String)
    role = Column(String) # admin, comptable, caissier, markting, investisseur
    etat_password = Column(String)
    statut = Column(String)
    pourcentage = Column(String)
    montant = Column(String)
    salaire = Column(String)
    conseil_admin = Column(String)
    boutique_id = Column(Integer, ForeignKey('boutique.id'))
    boutique = relationship("Boutiques", backref="boutique")

    

    def __init__(self, prenom, nom, telephone, mail, pass_word, adresse, region, code, cni, datenaissance, role, etat_password, statut, pourcentage, montant, salaire, conseil_admin, boutique):
        self.prenom = prenom
        self.nom = nom
        self.telephone = telephone
        self.mail = mail
        self.pass_word = pass_word
        self.adresse = adresse
        self.region = region
        self.code = code
        self.cni = cni
        self.datenaissance = datenaissance
        self.role = role
        self.etat_password = etat_password
        self.statut = statut  # actif, supprimer
        self.pourcentage = pourcentage
        self.montant = montant 
        self.salaire = salaire 
        self.conseil_admin = conseil_admin  #oui, non   
        self.boutique = boutique
