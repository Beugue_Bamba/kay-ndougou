# coding=utf-8

from sqlalchemy import Column, String, Integer, Numeric, Date, ForeignKey
from sqlalchemy.orm import relationship

from basepoissons import Base

class Mypanier(Base):
    __tablename__ = 'mypanier'

    id = Column(Integer, primary_key=True)
    
    values = Column(String)
    code = Column(String)

    def __init__(self, values, code):
        self.values = values
        self.code = code
        
